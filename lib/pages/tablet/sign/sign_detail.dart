import 'dart:io';
import 'package:advisor/components/button.dart';
import 'package:advisor/components/errormessage.dart';
import 'package:advisor/components/field.dart';
import 'package:advisor/components/showcamerabutton.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/main.dart';
import 'package:advisor/providers/signcontroller.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:advisor/requests/sharedprefrences.dart';
import 'package:advisor/requests/sqlite/sqlService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class SignDetailTab extends StatelessWidget {
  final SignController signController;
  final PageController pageController;
  SignDetailTab({this.signController,this.pageController});


  void showCameraButton(BuildContext context,SignController controller) {
    ShowCameraButtons.showBottomSheetCameraButton(context, (file) {
      controller.onChangeImage(file);
    });
  }

  Color getIconColor(int i){
    switch(i){
      case 1: return Colors.white;
      case 2: return Colors.red[500];
      case 3: return Colors.green[500];
    }
    return Colors.white;
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(
        child: Builder(
          builder:(context)=> Stack(
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: const EdgeInsets.all(8.0),
                        child: Stack(
                          children: [
                            Center(
                              child: GestureDetector(
                                onTap: () {
                                  showCameraButton(context,signController);
                                },
                                child: signController.image != null? CircleAvatar(
                                  radius: 70,
                                  backgroundColor: Colors.black26,
                                  backgroundImage: FileImage(signController.image),
                                ): CircleAvatar(
                                  radius: 70,
                                  backgroundColor: Colors.black26,
                                  backgroundImage: signController.imgLink == null ?null:signController.imgLink !='0'?NetworkImage(MyRequest.baseUrl+signController.imgLink):null,
                                  child: signController.imgLink != null && signController.imgLink !='0'?Container():Center(
                                    child: Icon(
                                      Icons.account_circle,
                                      size: 120,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Center(
                        child: Field(
                          label: 'اسم',
                          iconData: Icons.person,
                          onChange: signController.onChangeName,
                          iconColor: getIconColor(signController.checkName),
                          initValue: signController.name.trim() == 'not fount'?'':signController.name,
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Button(
                        title: 'بزن بریم',
                        onPress: ()async{
                          bool check = signController.validateName();

                          if(check){
                            String id = await MyRequest.insert({'name':signController.name,'phone':signController.phone}, 'signUser.php');
                            if(id != '-1'){
                              String profileAddress = signController.imgLink == null?'0':signController.imgLink;
                              if(signController.image != null){
                                String image = signController.getImageString(File(signController.image.path));
                                String imgName = signController.getImageName(id);
                                String pImg = signController.imgLink == null?'0':signController.imgLink;

                                Map<String, String> map = {
                                  'mob': signController.phone,
                                  'pimg': pImg,
                                  'img': image,
                                  'imgname': imgName
                                };
                                String result = await MyRequest.update(map, 'insertProfileSign.php');
                                if(result != '-1'){
                                  profileAddress = 'productimage/'+imgName+'.JPG';
                                  Map<String , dynamic> map = {'ID':int.parse(id),'name':signController.name,'profileaddress':profileAddress,'mobilenumber':signController.phone,'description':'0'};
                                  await SqLiteService().insert(map);
                                  // await SqLiteService().getUser();
                                  MySharedPreferences.insertSign();
                                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyApp()));
                                  //done
                                }else{
                                  ShowMessages.error('لطفا وضعیت اینترنت خود را چک کنید.', context);
                                }
                              }else{
                                Map<String , dynamic> map = {'ID':int.parse(id),'name':signController.name,'profileaddress':profileAddress,'mobilenumber':signController.phone,'description':'0'};
                                await SqLiteService().insert(map);
                                // await SqLiteService().getUser();
                                MySharedPreferences.insertSign();
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyApp()));
                              }
                            }else{
                              ShowMessages.error('لطفا وضعیت اینترنت خود را چک کنید.', context);
                            }
                          }else{
                            ShowMessages.error('اسم بیش از 2 کلمه وارد کنید.', context);
                          }
                        },
                      ),
                      SizedBox(
                        height: 100,
                      ),

                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
