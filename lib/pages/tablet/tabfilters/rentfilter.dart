
import 'package:advisor/city_and_states/city_and_states.dart';
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/button.dart';
import 'package:advisor/components/minifield.dart';
import 'package:advisor/components/add_widgets.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RentTabFilter extends StatefulWidget {

  createState() => RentTabFilterState();
}

class RentTabFilterState extends State<RentTabFilter> {

  int val = 1;
  int city = 1;
  int buyOrRent = 1;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: SimpleTextWhit(text:'فیلتر اجاره'),
          backgroundColor: AppColor.textColor,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [

              Container(
                margin: EdgeInsets.only(right: 20,top: 15),
                child: Row(
                  children: [
                    SimpleText(text:'استان : '),
                    SizedBox(width: 10,),
                    DropdownButton(
                      value: val,
                      items: CreateListDropDown().dropDownList(CityAndStates().states),
                      onChanged: (choose){
                        setState(() {
                          val = choose;
                        });
                      },
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 20,top: 15),
                child: Row(
                  children: [
                    SimpleText(text:'شهر : '),
                    SizedBox(width: 10,),
                    DropdownButton(
                      value: city,
                      items: CreateListDropDown().dropDownList(CityAndStates().hormozgan),
                      onChanged: (choose){
                        setState(() {
                          city = choose;
                        });
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),

              Row(
                children: [
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(label: 'شروع قیمت',onChange: (val){},)),),
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(label: 'پایان قیمت',onChange: (val){},)),),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(label: 'متراژ از',onChange: (val){},)),),
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(label: 'تا متراژ',onChange: (val){},)),),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  Expanded(child:Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(label: 'پارکینگ',onChange: (val){},)),),
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(label: 'چند خواب',onChange: (val){},)),),
                ],
              ),
              SizedBox(height: 40,),
              Button(title: 'اعمال فیلتر',onPress: (){},),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
