import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/other.dart';
import 'package:advisor/components/rent_mob_cart.dart';
import 'package:advisor/components/searchfieldwithoutlable.dart';
import 'package:advisor/pages/mobile/mobfilters/filteractions.dart';
import 'package:advisor/pages/mobile/mobfilters/rentfilter.dart';
import 'package:advisor/providers/Rentcontroller.dart';
import 'package:advisor/providers/filtercontroller.dart';
import 'package:advisor/moudles/Rent.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

class RentTab extends StatelessWidget {
  // final List<Rent> rents;
  // RentTab({this.rents});

  Widget searchWidgets(BuildContext context, RentController pageController) {
    return Container(
        height: 70,
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.black54,
            blurRadius: 15.0,
            offset: Offset(0.0, 0.75),
          )
        ]),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 5,
                child: Center(
                    child: SearchFieldWithOutLabel(
                  onChange: pageController.onChangeSearchText,
                  iconData: Icons.search,
                )),
              ),
              Expanded(
                  flex: 1,
                  child: Center(
                      child: IconButton(
                          onPressed: () {
                            OtherWidgets.showSortingOption(
                                context, pageController.changeSortIndex, () {
                              Navigator.pop(context);
                            }, pageController.sortIndex,
                                pageController.sortingOption);
                          },
                          icon: Icon(
                            Icons.sort,
                            color: AppColor.textColor,
                          )))),
              Expanded(
                  flex: 1,
                  child: Center(
                      child: IconButton(
                          onPressed: () async {
                            FilterController filterController =
                                new FilterController();
                            filterController
                                .updateFilter(pageController.filters);
                            Map<String, dynamic> filters = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ChangeNotifierProvider.value(
                                  value: filterController,
                                  child: RentFilterMob(),
                                ),
                              ),
                            );
                            if (filters != null) {
                              pageController.updateFilters(filters);
                            }
                          },
                          icon: Icon(
                            MdiIcons.filter,
                            color: AppColor.textColor,
                          )))),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    RentController pageController = Provider.of<RentController>(context);
    List<Rent> rents = pageController.searchRents;
    FilterAction filterActions = FilterAction(filters: pageController.filters);
    rents = filterActions.filterActionsPriceRent(rents);
    rents = filterActions.filterActionsAreaRent(rents);
    rents = filterActions.filterActionsBedroomRent(rents);
    rents = filterActions.filterActionsParkingRent(rents);

    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            searchWidgets(context, pageController),
            Wrap(
              children: List.generate(
                rents.length,
                    (index) => FutureProvider.value(
                  value: MyRequest.getMyProductImageLink(
                      rents[index].id, rents[index].status),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: RentMobCart(
                      rent: rents[index],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
