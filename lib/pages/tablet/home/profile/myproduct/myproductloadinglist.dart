import 'package:advisor/components/userservicecart/user_service_cart_loading.dart';
import 'package:flutter/cupertino.dart';

class MyLoadingProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(10, (index) => UserServiceCartLoading()),
    );
  }
}
