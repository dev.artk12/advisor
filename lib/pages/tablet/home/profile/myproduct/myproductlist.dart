import 'package:advisor/components/userservicecart/user_service_cart.dart';
import 'package:advisor/components/userservicecart/user_service_cart_loading.dart';
import 'package:advisor/components/userservicecart/user_service_cart_tab.dart';
import 'package:advisor/moudles/user.dart';
import 'package:advisor/providers/Rentcontroller.dart';
import 'package:advisor/providers/profilecontroller.dart';
import 'package:advisor/moudles/product.dart';
import 'package:advisor/providers/saledcontroller.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class MyProductList extends StatelessWidget {
  final List<Product> products;
  final User user;
  final SalesController salesController;
  final RentController rentController;
  MyProductList({this.products,this.user,this.salesController,this.rentController});

  @override
  Widget build(BuildContext context) {
    ProfileController profileController = context.watch<ProfileController>();

    return Column(
      children: List.generate(products.length, (index) {
        Product product = products[index];
        String status = product.status == '1' ? 'فروش' : 'اجاره';

        return FutureProvider.value(
          value: MyRequest.getMyProductImageLink(product.id,product.status),
          child: UserServiceCartTab(
            rentController: rentController,
            salesController: salesController,
            profileController:profileController,
            area: product.area,
            bedRooms: product.bedroom,
            parking: product.parking,
            price: product.price,
            status: status,
            id: product.id,
            address: product.address,
            city: product.city,
            description: product.description,
            state: product.state,
            statusId: product.status,
            bathroom: product.bathroom,
            user:user
          ),
        );
      }),
    );
  }
}
