import 'dart:convert';
import 'dart:io';
import 'package:advisor/city_and_states/city_and_states.dart';
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/add_widgets.dart';
import 'package:advisor/components/addressfield.dart';
import 'package:advisor/components/button.dart';
import 'package:advisor/components/descriptionproductfield.dart';
import 'package:advisor/components/descriptionprofilefield.dart';
import 'package:advisor/components/errormessage.dart';
import 'package:advisor/components/minifield.dart';
import 'package:advisor/components/showcamerabutton.dart';
import 'package:advisor/moudles/imageid.dart';
import 'package:advisor/moudles/product.dart';
import 'package:advisor/moudles/user.dart';
import 'package:advisor/providers/Rentcontroller.dart';
import 'package:advisor/providers/profilecontroller.dart';
import 'package:advisor/providers/productpagecontroller.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:advisor/providers/saledcontroller.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:digit_to_persian_word/digit_to_persian_word.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:provider/provider.dart';

class EditHomeTab extends StatelessWidget {
  final ProfileController profileController;
  final User user;
  final SalesController salesController;
  final RentController rentController;
  EditHomeTab({this.profileController, this.user,this.salesController,this.rentController});

  Widget addPictureButton(ProductController controller, BuildContext context) {
    return Button(
      title: 'اضافه کردن عکس',
      onPress: () {
        ShowCameraButtons.showBottomSheetCameraButton(context, (file) {
          ImageId img = new ImageId(image: file, id: controller.images.length);
          controller.addImage(img);
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    ProductController controller = Provider.of<ProductController>(context);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: SimpleTextWhit(text: 'ویرایش'),
          backgroundColor: AppColor.textColor,
        ),
        body: Builder(
          builder: (context) => SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(15),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(
                            MdiIcons.noteText,
                            color: AppColor.textColor,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                              child: SimpleText(
                                  text:
                                      'نکته : انتخاب عکس اول به عنوان عکس کاور به مشتریان در صفحه اصلی نمایش داده میشود.')),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Icon(
                            MdiIcons.noteText,
                            color: AppColor.textColor,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                              child: SimpleText(
                                  text:
                                      'نکته : در صورت انتخاب اجاره، لطفا قیمت را ماهانه وارد کنید. ')),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                controller.images.length + controller.imagesLink.length == 0
                    ? Container()
                    : addPictureButton(controller,context),
                SizedBox(
                  height: 10,
                ),
                controller.images.length + controller.imagesLink.length == 0
                    ? Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.camera,
                                  color: AppColor.textColor,
                                ),
                                Button(
                                    onPress: () async {
                                      final pickedFile = await ImagePicker()
                                          .getImage(source: ImageSource.camera);
                                      ImageId img = new ImageId(
                                          image: File(pickedFile.path),
                                          id: controller.images.length);
                                      controller.addImage(img);
                                    },
                                    title: 'آپلود با دوربین'),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.image,
                                  color: AppColor.textColor,
                                ),
                                Button(
                                    onPress: () async {
                                      final pickedFile = await ImagePicker()
                                          .getImage(
                                              source: ImageSource.gallery);
                                      ImageId img = new ImageId(
                                          image: File(pickedFile.path),
                                          id: controller.images.length);
                                      controller.addImage(img);
                                    },
                                    title: 'آپلود با گالری'),
                              ],
                            ),
                          ],
                        ),
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(
                            left: 30, right: 30, top: 15, bottom: 15),
                        height: 200,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.textColor,
                          ),
                        ),
                      )
                    : Container(
                        width: MediaQuery.of(context).size.width,
                        height: 200,
                        child: CarouselSlider.builder(
                            itemCount: controller.images.length +
                                controller.imagesLink.length,
                            options: CarouselOptions(
                                autoPlay: false,
                                enlargeCenterPage: true,
                                viewportFraction: 0.9,
                                aspectRatio: 0.1,
                                initialPage: 0,
                                enableInfiniteScroll: true),
                            itemBuilder: (context, index) {
                              return Container(
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        color: Colors.black38,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            IconButton(
                                                onPressed: () async {
                                                  if (index <
                                                      controller
                                                          .imagesLink.length) {
                                                    //TODO : Dialog
                                                    ProductImageLink p =
                                                        controller
                                                            .imagesLink[index];
                                                    // print(controller.status);
                                                    // String result = await MyRequest
                                                    //     .deleteProductImage(p.id, p.link,controller.status.toString());
                                                    controller.deleteImgLink(
                                                        p.id, p.link);
                                                  } else {
                                                    controller.deleteImage(
                                                        controller
                                                            .images[index -
                                                                controller
                                                                    .imagesLink
                                                                    .length]
                                                            .image
                                                            .path);
                                                  }
                                                },
                                                icon: Icon(
                                                  Icons.delete,
                                                  color: Colors.white,
                                                )),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.only(
                                    left: 30, right: 30, top: 15, bottom: 15),
                                height: 200,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(1),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: Colors.transparent,
                                  ),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: controller.firstImageLoadLength == 0
                                        ? AssetImage(
                                            'assets/images/loading.gif')
                                        : index < controller.imagesLink.length
                                            ? NetworkImage(MyRequest.baseUrl +
                                                controller
                                                    .imagesLink[index].link)
                                            : FileImage(controller
                                                .images[index -
                                                    controller
                                                        .imagesLink.length]
                                                .image),
                                  ),
                                ),
                              );
                            }),
                      ),
                // Container(
                //   margin: EdgeInsets.only(right: 20, top: 15),
                //   child: Row(
                //     children: [
                //       SimpleText(text: 'برای* : '),
                //       SizedBox(
                //         width: 10,
                //       ),
                //       DropdownButton(
                //         value: controller.status,
                //         items: CreateListDropDown()
                //             .dropDownList(CityAndStates().buyOrRent),
                //         onChanged: controller.changeStatus,
                //       ),
                //     ],
                //   ),
                // ),
                Container(
                  margin: EdgeInsets.only(right: 20, top: 15),
                  child: Row(
                    children: [
                      SimpleText(text: 'استان* : '),
                      SizedBox(
                        width: 10,
                      ),
                      DropdownButton(
                        value: controller.state,
                        items: CreateListDropDown()
                            .dropDownList(CityAndStates().states),
                        onChanged: controller.changeState,
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 20, top: 15),
                  child: Row(
                    children: [
                      SimpleText(text: 'شهر* : '),
                      SizedBox(
                        width: 10,
                      ),
                      DropdownButton(
                        value: controller.city,
                        items: CreateListDropDown().dropDownList(
                            CityAndStates().getStateCity(controller.state)),
                        onChanged: controller.changeCity,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Row(
                    children: [
                      SimpleText(text: 'پارکینگ : '),
                      SimpleText(
                          text: controller.parkingCheck ? 'دارد' : 'ندارد'),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        // width: 100,
                        child: Switch(
                          value: controller.parkingCheck,
                          activeColor: AppColor.textColor,
                          onChanged: controller.updateParkingCheck,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                controller.price == null
                    ? Container()
                    : Padding(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, bottom: 8, top: 8),
                  child: Row(
                    children: [
                      Expanded(
                          child: SimpleText(
                            text: DigitToWord.toWord(
                                NumberUtility.changeDigit(
                                    controller.price, NumStrLanguage.English),
                                StrType.StrWord,
                                isMoney: true),
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                          child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: MiniField(
                          label: 'قیمت به تومان*',
                          onChange: controller.changePrice,
                          textInputType: TextInputType.number,
                          initText: NumberUtility.changeDigit(controller.price, NumStrLanguage.Farsi),
                        ),
                      )),
                    ),
                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: MiniField(
                            label: 'تعداد سرویس بهداشتی*',
                            onChange: controller.changeBathroom,
                            textInputType: TextInputType.number,
                            initText: NumberUtility.changeDigit(controller.bathroom, NumStrLanguage.Farsi),
                          )),
                    ),
                    // Expanded(child: Text('')),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
//                  Expanded(child:Text('')),
                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: MiniField(
                            label: 'متراژ خونه*',
                            onChange: controller.changeArea,
                            textInputType: TextInputType.number,
                            initText: NumberUtility.changeDigit(controller.area, NumStrLanguage.Farsi),
                          )),
                    ),

                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          child: MiniField(
                              label: 'چند خواب*',
                              onChange: controller.changeBedRoom,
                              initText: NumberUtility.changeDigit(controller.bedRoom, NumStrLanguage.Farsi),
                              textInputType: TextInputType.number)),
                    ),
//                  Expanded(child:Text('')),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                AddressField(
                    label: 'ادرس*',
                    onChange: controller.changeAddress,
                    initText: controller.address),
                SizedBox(
                  height: 10,
                ),
                DescriptionProductField(
                    label: 'توضیحات',
                    onChange: controller.changeDescription,
                    initText: controller.description),
                SizedBox(
                  height: 30,
                ),
                Button(
                  title: 'ذخیره',
                  onPress: () async {

                    String status = '${controller.status}';
                    String state = '${controller.state}';
                    String city = '${controller.city}';
                    String price = controller.price;
                    String area = controller.area;
                    String parking = controller.parking;
                    String bedroom = controller.bedRoom;
                    String address = controller.address;
                    String description = controller.description;
                    List<ImageId> images = controller.images;
                    String bathroom = controller.bathroom;
                    List<ProductImageLink> imageLinks = controller.imagesLink;

                    String checkResult = AddProduct().checkFiledEdit(price,
                        area, parking, bedroom, address, description,
                        images, imageLinks, bathroom);

                    String stateName =
                    CityAndStates().states[controller.state - 1];
                    String cityName = CityAndStates()
                        .getStateCity(controller.state)[controller.city - 1];

                    if (checkResult != 'done') {
                      AddProduct().showAddErrorMessage(checkResult, context);
                    } else {
                      ShowMessages.loading('کمی صبر کنید...', context);
                      String res = await AddProduct().editProduct(controller.id, status,
                          state, city, price, area, parking, bedroom, address, description,
                          user.id, bathroom, stateName, cityName, controller.deleteImagedId);

                      if (res == '-1') {
                        ShowMessages.error(
                            'لطفا وضغیت اینترنت رو چک کنید.', context);
                      } else {
                        controller.updateProductId(res);
                        while(controller.i < images.length){
                          DateTime dateTime = DateTime.now();
                          String now =
                              '${dateTime.year}${dateTime.month}${dateTime.day}${dateTime.hour}${dateTime.minute}${dateTime.second}${dateTime.millisecond}';
                          String name = now + '_{${controller.i}}_${controller.productId}';
                          File image = controller.images[controller.i].image;
                          List<int> imageBytes = image.readAsBytesSync();
                          String base64Image = base64Encode(imageBytes);
                          String res = await AddProduct()
                              .insertImage(base64Image, name, controller.productId, status);

                          if(res == '-1'){
                            controller.updateRequest(false);
                            imageLinks.add(ProductImageLink(
                                id: res,
                                productId: controller.id,
                                link: 'productimage/$name.JPG'));
                            ShowMessages.error('لطفا وضغیت اینترنت رو چک کنید.', context);
                            break;
                          }else{
                            controller.updateI();
                          }
                        }

                        if(controller.i == controller.images.length && controller.productId != null){
                          Product p = Product(id: controller.id, state: state, city: city, cityName: cityName,
                            description: description, price: price, area: area, address: address,
                            bedroom: bedroom, parking: parking, stateName: stateName, bathroom: bathroom,
                          );
                          profileController.editProduct(p);
                          if (status == '1') {
                            salesController.editMyProductInList(p);
                          } else {
                            rentController.editMyProductInList(p);
                          }
                          Navigator.pop(context);
                        }
                      }
                    }
                  },
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
