import 'package:advisor/pages/loading.dart';
import 'package:advisor/moudles/Rent.dart';
import 'package:advisor/moudles/sales.dart';
import 'package:advisor/moudles/user.dart';
import 'package:advisor/pages/mobile/home/home.dart';
import 'package:advisor/pages/mobile/sign/signPages.dart';
import 'package:advisor/pages/tablet/home/home.dart';
import 'package:advisor/pages/tablet/sign/signPages.dart';
import 'package:advisor/providers/signcontroller.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:advisor/requests/sqlite/sqlService.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  String getDeviceType() {
    final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
    return data.size.shortestSide < 600 ? 'phone' : 'tablet';
  }

  @override
  Widget build(BuildContext context) {
    bool sign = Provider.of<bool>(context);
    List<Sale> sales = Provider.of<List<Sale>>(context) ?? [];
    List<Rent> rents = Provider.of<List<Rent>>(context) ?? [];

    String device = getDeviceType();
    if (sign == null) {
      return LoadingPage();
    } else {
      if (sign) {
        return FutureBuilder(
            future: SqLiteService().getUser(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                User user = snapshot.data;
                if (device == "phone") {
                  return MultiProvider(
                    providers: [
                      FutureProvider(create: (_) => MyRequest.getMyProduct(user.id)),
                      FutureProvider(create: (_) => MyRequest.getUser(user.id)),
                    ],
                      child: HomeMobile(
                        rents: rents,
                        sales: sales,
                      ));
                } else {
                  return FutureProvider(
                      create: (_) => MyRequest.getMyProduct(user.id),
                      child: HomeTab(
                        rents: rents,
                        sales: sales,
                        user: user,
                      ));
                }
              } else {
                return LoadingPage();
              }
            });
      } else {
        if (device == "phone") {
          return ChangeNotifierProvider.value(
              value: SignController(), child: SignMob());
        } else {

          return ChangeNotifierProvider.value(
              value: SignController(), child: SignTab());
        }
      }
    }
  }
}
