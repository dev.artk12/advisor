import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/other.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/components/simpletextbiggerfont.dart';
import 'package:advisor/components/simpletextprice.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:digit_to_persian_word/digit_to_persian_word.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:sliding_sheet/sliding_sheet.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ProductPage extends StatefulWidget {
  final List<ProductImageLink> imageLinks;
  final String status;
  final String price;
  final String area;
  final String bedroom;
  final String bathroom;
  final String description;
  final String mobile;
  final String name;
  final String profileAddress;

  ProductPage(
      {this.imageLinks,
      this.status,
      this.price,
      this.area,
      this.bedroom,
      this.bathroom,
      this.description,
      this.mobile,
      this.name,
      this.profileAddress});

  createState() => ProductPageState();
}

class ProductPageState extends State<ProductPage> {
  int page = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SlidingSheet(
          elevation: 8,
          cornerRadius: 16,
          snapSpec: const SnapSpec(
            snap: true,
            snappings: [0.6, 0.7, 1.0],
            positioning: SnapPositioning.relativeToAvailableSpace,
          ),
          body: Stack(
            children: [
              CarouselSlider(
                  items: List.generate(widget.imageLinks.length, (index)=>
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (cntx) => Material(
                                  color: Colors.transparent,
                                  child: Stack(
                                    children: [
                                      Container(
                                        child: Center(
                                          child: CarouselSlider(
                                            options: CarouselOptions(
                                              height:500,
                                              initialPage: 0,
                                              enableInfiniteScroll: true,
                                              reverse: false,
                                              enlargeCenterPage: false,
                                              scrollDirection:
                                                  Axis.horizontal,
                                            ),
                                            items: List.generate(widget.imageLinks.length, (index) => Container(
                                              child: Center(
                                                child: FadeInImage.assetNetwork(fit: BoxFit.cover,image: MyRequest.baseUrl+widget.imageLinks[index].link,
                                                placeholder:'assets/images/loading.gif',),
                                              ),
                                            ),)
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Container(
                                            margin: EdgeInsets.all(10),
                                            child: InkWell(
                                                onTap: () =>
                                                    Navigator.pop(cntx),
                                                child: Icon(
                                                  Icons.close,
                                                  color: Colors.white,
                                                ))),
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: Container(
                                            margin: EdgeInsets.all(10),
                                            child: Icon(
                                              Icons.arrow_back_ios,
                                              color: Colors.white,
                                            )),
                                      ),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                            margin: EdgeInsets.all(10),
                                            child: Icon(
                                              Icons.arrow_forward_ios,
                                              color: Colors.white,
                                            )),
                                      ),
                                    ],
                                  ),
                                ));
                      },
                      child: Container(
                        height: MediaQuery.of(context).size.height / 2,
                        child: Stack(
                          children: [
                            Container(height: MediaQuery.of(context).size.height / 3,child: Center(child:Image.asset('assets/images/loading.gif'))),
                            Container(decoration: BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(MyRequest.baseUrl+widget.imageLinks[index].link),
                                )),)
                          ],
                        ),
                        // decoration: BoxDecoration(
                        //     image: DecorationImage(
                        //   fit: BoxFit.cover,
                        //   image: AssetImage('assets/images/phone_sign.jpg'),
                        // )),
                        // child: Image.asset('assets/images/phone_sign.jpg',fit: BoxFit.fitWidth,),
                      ),
                    ),
                  ),
                  options: CarouselOptions(
                    height: 400,
                    // aspectRatio: 16/9,
                    viewportFraction: 1.0,
                    initialPage: 0,
                    onPageChanged: (i, p) {
                      setState(() {
                        page = i;
                      });
                    },
                    enableInfiniteScroll: true,
                    reverse: false,
                    autoPlay: true,
                    autoPlayInterval: Duration(seconds: 5),
                    autoPlayAnimationDuration: Duration(seconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal,
                  )),
              Align(
                alignment: Alignment.topRight,
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height / 2.7,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: new DotsIndicator(
                    dotsCount: widget.imageLinks.length,
                    position: (widget.imageLinks.length-1) - page + 0.0,
                    decorator: DotsDecorator(
                      size: const Size.square(9.0),
                      activeSize: const Size(18.0, 9.0),
                      activeShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                ),
              ),
            ],
          ),
          builder: (context, state) {
            return Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: SimpleTextBiggerFont(
                    text: widget.status == '1'?'فروش':'اجاره',
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                OtherWidgets.dividerWithCenterText('مشخصات قطعه'),
                SizedBox(
                  height: 20,
                ),
                Center(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          'assets/images/money.svg',
                          height: 25,
                          width: 25,
                        ),
                        SizedBox(
                          width: 7,
                        ),
                        SimpleTextPrice(
                            text: NumberUtility.changeDigit(
                                widget.price, NumStrLanguage.Farsi) +
                                ' تومان'),
                        widget.status == '2'?SimpleText(text:' (درماه) '):Container(),

                      ],
                    )),
                SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SimpleText(text:'معادل : '),
                      SizedBox(width: 5,),
                      SimpleText(text: DigitToWord.toWord(
                          NumberUtility.changeDigit(
                              widget.price, NumStrLanguage.English),
                          StrType.StrWord,
                          isMoney: true),),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/images/area.svg',
                        height: 25,
                        width: 25,
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      SimpleText(
                          text: NumberUtility.changeDigit(
                              widget.area, NumStrLanguage.Farsi) +
                              ' متر '),
                    ],
                  ),
                ),
                // Row(
                //   children: [
                //     // Expanded(
                //     //   flex: 4,
                //     //   child:
                //     // ),
                //     Expanded(
                //       flex: 1,
                //       child: Text(''),
                //     ),
                //     Expanded(
                //       flex: 3,
                //       child:
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 15,
                ),
                Center(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.hot_tub,
                          color: AppColor.textColor,
                        ),
                        SizedBox(
                          width: 7,
                        ),
                        SimpleText(
                            text: NumberUtility.changeDigit(
                                widget.bathroom, NumStrLanguage.Farsi) +
                                ' سرویس بهداشتی'),
                      ],
                    )),
                SizedBox(
                  height: 15,
                ),
                Center(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.hotel,
                          color: AppColor.textColor,
                        ),
                        SizedBox(
                          width: 7,
                        ),
                        SimpleText(
                            text: NumberUtility.changeDigit(
                                widget.bedroom, NumStrLanguage.Farsi) +
                                ' خواب'),
                      ],
                    )),
                // Row(
                //   children: [
                //     Expanded(
                //       flex: 4,
                //       child:
                //     ),
                //     Expanded(
                //       flex: 1,
                //       child: Text(''),
                //     ),
                //     Expanded(
                //       flex: 3,
                //       child:
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 20,
                ),
                widget.description == null ? Container():widget.description.isEmpty? Container():OtherWidgets.dividerWithCenterText('توضیحات قطعه'),
                widget.description == null ? Container():widget.description.isEmpty? Container(): SizedBox(
                  height: 15,
                ),
                widget.description == null ? Container():widget.description.isEmpty? Container():Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                          child: SimpleText(
                              text: widget.description)),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                OtherWidgets.dividerWithCenterText('مشخصات شخص بارگذارکننده'),
                SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      backgroundColor: AppColor.textColor,
                      backgroundImage: widget.profileAddress == '0'?null:NetworkImage(MyRequest.baseUrl+widget.profileAddress),
                      child: widget.profileAddress != '0'?null:Center(
                        child: Center(child: Icon(Icons.person,color: Colors.white,),),
                      ),
                      radius: 50,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SimpleText(text: widget.name),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 1,
                          width: 150,
                          color: Colors.black54,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        SimpleText(text: NumberUtility.changeDigit(widget.mobile, NumStrLanguage.Farsi)),
                      ],
                    )
                  ],
                ),
                Container(
                  height: MediaQuery.of(context).size.height/3,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
