
import 'package:advisor/moudles/Rent.dart';
import 'package:advisor/moudles/sales.dart';

class FilterAction {

  final Map<String,dynamic> filters;

  FilterAction({this.filters});


  //--------------------------sale------------------------

  List<Sale> filterActionsPrice(List<Sale> searchSales) {
    String start = filters['startPrice']??'';
    String end = filters['endPrice']??'';
    List<Sale> filterSale = [];
    if (start.isEmpty && end.isEmpty) {
      filterSale = searchSales;
    } else if (start.isNotEmpty && end.isEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.price) == int.parse(start))
          .toList();
    } else if (start.isEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.price) == int.parse(end))
          .toList();
    } else if (start.isNotEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) =>
      int.parse(element.price) >= int.parse(start) &&
          int.parse(element.price) >= int.parse(end))
          .toList();
    }
    return filterSale;
  }

  List<Sale> filterActionsArea(List<Sale> searchSales) {
    String start = filters['startArea']??'';
    String end = filters['endArea']??'';
    List<Sale> filterSale = [];
    if (start.isEmpty && end.isEmpty) {
      filterSale = searchSales;
    } else if (start.isNotEmpty && end.isEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.area) == int.parse(start))
          .toList();
    } else if (start.isEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.area) == int.parse(end))
          .toList();
    } else if (start.isNotEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) =>
      int.parse(element.area) >= int.parse(start) &&
          int.parse(element.area) <= int.parse(end))
          .toList();
    }
    return filterSale;
  }

  List<Sale> filterActionsBedroom(List<Sale> searchSales) {
    String start = filters['startBedroom']??'';
    String end = filters['endBedroom']??'';
    List<Sale> filterSale = [];
    if (start.isEmpty && end.isEmpty) {
      filterSale = searchSales;
    } else if (start.isNotEmpty && end.isEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.bedroom) == int.parse(start))
          .toList();
    } else if (start.isEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.bedroom) == int.parse(end))
          .toList();
    } else if (start.isNotEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) =>
      int.parse(element.bedroom) >= int.parse(start) &&
          int.parse(element.bedroom) <= int.parse(end))
          .toList();
    }
    return filterSale;
  }

  List<Sale> filterActionsParking(List<Sale> searchSales) {
    bool parking = filters['parking']??false;
    bool noParking = filters['noParking']??false;
    List<Sale> filterSale = [];
    if (!parking && !noParking) {
      filterSale = searchSales;
    } else if (parking && !noParking) {

      filterSale = searchSales
          .where((element) => int.parse(element.parking) > 0)
          .toList();
    } else if (!parking && noParking) {
      filterSale = searchSales
          .where((element) => int.parse(element.parking) == 0)
          .toList();
    } else if (parking && noParking) {
      filterSale = searchSales;
    }
    return filterSale;
  }


  //--------------------------------------rent---------------------------

  List<Rent> filterActionsPriceRent(List<Rent> searchSales) {
    String start = filters['startPrice']??'';
    String end = filters['endPrice']??'';
    List<Rent> filterSale = [];
    if (start.isEmpty && end.isEmpty) {
      filterSale = searchSales;
    } else if (start.isNotEmpty && end.isEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.price) == int.parse(start))
          .toList();
    } else if (start.isEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.price) == int.parse(end))
          .toList();
    } else if (start.isNotEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) =>
      int.parse(element.price) >= int.parse(start) &&
          int.parse(element.price) >= int.parse(end))
          .toList();
    }
    return filterSale;
  }

  List<Rent> filterActionsAreaRent(List<Rent> searchSales) {
    String start = filters['startArea']??'';
    String end = filters['endArea']??'';
    List<Rent> filterSale = [];
    if (start.isEmpty && end.isEmpty) {
      filterSale = searchSales;
    } else if (start.isNotEmpty && end.isEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.area) == int.parse(start))
          .toList();
    } else if (start.isEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.area) == int.parse(end))
          .toList();
    } else if (start.isNotEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) =>
      int.parse(element.area) >= int.parse(start) &&
          int.parse(element.area) <= int.parse(end))
          .toList();
    }
    return filterSale;
  }

  List<Rent> filterActionsBedroomRent(List<Rent> searchSales) {
    String start = filters['startBedroom']??'';
    String end = filters['endBedroom']??'';
    List<Rent> filterSale = [];
    if (start.isEmpty && end.isEmpty) {
      filterSale = searchSales;
    } else if (start.isNotEmpty && end.isEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.bedroom) == int.parse(start))
          .toList();
    } else if (start.isEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) => int.parse(element.bedroom) == int.parse(end))
          .toList();
    } else if (start.isNotEmpty && end.isNotEmpty) {
      filterSale = searchSales
          .where((element) =>
      int.parse(element.bedroom) >= int.parse(start) &&
          int.parse(element.bedroom) <= int.parse(end))
          .toList();
    }
    return filterSale;
  }

  List<Rent> filterActionsParkingRent(List<Rent> searchSales) {
    bool parking = filters['parking']??false;
    bool noParking = filters['noParking']??false;
    List<Rent> filterSale = [];
    if (!parking && !noParking) {
      filterSale = searchSales;
    } else if (parking && !noParking) {

      filterSale = searchSales
          .where((element) => int.parse(element.parking) > 0)
          .toList();
    } else if (!parking && noParking) {
      filterSale = searchSales
          .where((element) => int.parse(element.parking) == 0)
          .toList();
    } else if (parking && noParking) {
      filterSale = searchSales;
    }
    return filterSale;
  }
}