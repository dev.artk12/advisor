
import 'package:advisor/city_and_states/city_and_states.dart';
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/addressfield.dart';
import 'package:advisor/components/button.dart';
import 'package:advisor/components/descriptionprofilefield.dart';
import 'package:advisor/components/minifield.dart';
import 'package:advisor/components/other.dart';
import 'package:advisor/components/add_widgets.dart';
import 'package:advisor/providers/filtercontroller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BuyFilterMob extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    FilterController filterController = Provider.of<FilterController>(context);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: SimpleTextWhit(text:'فیلتر فروش'),
          backgroundColor: AppColor.textColor,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              OtherWidgets.divider('پارکینگ'),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.only(right: 30),
                child: Row(
                  children: [
                    SimpleText(text:'پارکینگ داشته باشه'),
                    Checkbox(
                      activeColor: AppColor.textColor,
                      onChanged: filterController.onChangeParking,
                      value: filterController.parking,
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 30),
                child: Row(
                  children: [
                    SimpleText(text:'پارکینگ نداشته باشه'),
                    Checkbox(
                      activeColor: AppColor.textColor,
                      onChanged: filterController.onChangeNoParking,
                      value: filterController.noParking,
                    ),
                  ],
                ),
              ),
              OtherWidgets.divider('قیمت'),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(textInputType: TextInputType.number,initText: filterController.startPrice,label: 'شروع قیمت',onChange: filterController.onChangeStartPrice,)),),
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(textInputType: TextInputType.number,initText: filterController.endPrice,label: 'پایان قیمت',onChange: filterController.onChangeEndPrice,)),),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              OtherWidgets.divider('متراژ'),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(textInputType: TextInputType.number,initText: filterController.startArea,label: 'متراژ از',onChange: filterController.onChangeStartArea,)),),
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(textInputType: TextInputType.number,initText: filterController.endArea,label: 'تا متراژ',onChange: filterController.onChangeEndArea,)),),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              OtherWidgets.divider('تعداد اتاق ها خواب'),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(child:Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(textInputType: TextInputType.number,initText: filterController.startBedroom,label: 'شروع',onChange: filterController.onChangeStartBedroom,)),),
                  Expanded(child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10),
                      child: MiniField(textInputType: TextInputType.number,initText: filterController.endBedroom,label: 'پایان',onChange: filterController.onChangeEndBedroom,)),),
                ],
              ),
              SizedBox(
                height: 20,
              ),

              SizedBox(height: 20,),
              Button(title: 'اعمال فیلتر',onPress: (){
                Navigator.pop(context,filterController.filter);
              },),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
