import 'dart:convert';
import 'dart:io';
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/addbutton.dart';
import 'package:advisor/components/button.dart';
import 'package:advisor/components/descriptionprofilefield.dart';
import 'package:advisor/components/minifield.dart';
import 'package:advisor/components/semicircle.dart';
import 'package:advisor/components/showcamerabutton.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/components/simpletextbiggerfont.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:advisor/providers/Rentcontroller.dart';
import 'package:advisor/providers/productpagecontroller.dart';
import 'package:advisor/providers/profilecontroller.dart';
import 'package:advisor/providers/saledcontroller.dart';
import 'package:advisor/moudles/product.dart';
import 'package:advisor/moudles/user.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:advisor/requests/sqlite/sqlService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:provider/provider.dart';
import 'myproduct/add.dart';
import 'myproduct/myproductlist.dart';
import 'myproduct/myproductloadinglist.dart';

class ProfileMob extends StatelessWidget {
  final RentController rentController;
  final SalesController salesController;
  final User user;

  ProfileMob({this.salesController, this.rentController, this.user});

  String getImageString(File image) {
    List<int> imageBytes = image.readAsBytesSync();
    return base64Encode(imageBytes);
  }

  String getImageName(String id) {
    DateTime dateTime = DateTime.now();
    return '${dateTime.year}${dateTime.month}${dateTime.day}${dateTime.hour}'
        '${dateTime.minute}${dateTime.second}${dateTime.millisecond}_profile_$id';
  }

  void addToPublicList(Product product, RentController rentController,
      SalesController salesController, User user) {
    if (product.status == '1') {
      salesController.addTOListFromMyProduct(
          product, user.name, user.mobileNumber, user.profileAddress);
    } else {
      rentController.addTOListFromMyProduct(
          product, user.name, user.mobileNumber, user.profileAddress);
    }
  }

  @override
  Widget build(BuildContext context) {
    ProfileController profileController =
        Provider.of<ProfileController>(context);

    void showCameraButton(BuildContext context) {
      ShowCameraButtons.showBottomSheetCameraButton(context, (file)async{
        String image = getImageString(file);
        String name = getImageName(user.id);
        String pImg = user.profileAddress;

        Map<String, String> map = {
          'id': user.id,
          'pimg': pImg.substring(pImg.indexOf('/') + 1),
          'img': image,
          'imgname': name
        };
        String result =
            await MyRequest.update(map, 'insertProfile.php');
        if (result == 'done') {
          user.profileAddress = 'productimage/$name.JPG';
          SqLiteService.update(
              {'profileaddress': 'productimage/$name.JPG'},
              int.parse(user.id));
          // user.profileAddress = 'productimage/$name.JPG';
          profileController.updateToRefresh();
        }
        // Navigator.pop(context);
      });
    }

    void showDialog(
        String initialText, String label, String title, User user, int i) {
      // String name = initialText;
      TextEditingController editingController =
          TextEditingController(text: initialText);
      String message = '';
      showGeneralDialog(
        barrierLabel: "$title",
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.5),
        transitionDuration: Duration(milliseconds: 700),
        context: context,
        pageBuilder: (_, __, ___) {
          return StatefulBuilder(
            builder: (context, setState) => SafeArea(
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: i == 1 ? 300 : 450,
                  child: ListView(
                    children: [
                      Material(
                        child: SizedBox(
                          height: 10,
                        ),
                        color: Colors.transparent,
                      ),
                      Material(
                        child:
                            Center(child: SimpleTextBiggerFont(text: '$title')),
                        color: Colors.transparent,
                      ),
                      Material(
                        child: SizedBox(
                          height: 60,
                        ),
                        color: Colors.transparent,
                      ),
                      Material(
                        child: Container(
                            margin: EdgeInsets.only(left: 5, right: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                i == 1
                                    ? MiniField(
                                        label: i == 1 ? 'اسم' : 'توضیحات',
                                        textAlign: TextAlign.center,
                                        editingController: editingController,
                                      )
                                    : DescriptionProfileField(
                                        label: i == 1 ? 'اسم' : 'توضیحات',
                                        editingController: editingController,
                                      ),
                                SizedBox(
                                  height: 30,
                                ),
                                SimpleText(
                                  text: message,
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Button(
                                  onPress: () async {
                                    String result;
                                    if (editingController.text.isEmpty) {
                                      setState(() {
                                        message = 'لطفا فیلد رو وارد کنید.';
                                      });
                                    } else {
                                      if (label == 'اسم') {
                                        result = await MyRequest.updateName(
                                            <String, String>{
                                              'id': user.id,
                                              'name': editingController.text
                                            },
                                            'updateName.php');
                                      } else {
                                        result = await MyRequest.updateName(
                                            <String, String>{
                                              'id': user.id,
                                              'des': editingController.text
                                            },
                                            'updateDescription.php');
                                      }
                                      if (result == 'done') {
                                        if (i == 1) {
                                          SqLiteService.update(
                                              {'name': editingController.text},
                                              int.parse(user.id));
                                          user.name = editingController.text;
                                        } else {
                                          SqLiteService.update({
                                            'description':
                                                editingController.text
                                          }, int.parse(user.id));
                                          user.description =
                                              editingController.text;
                                        }
                                        profileController.updateToRefresh();
                                        Navigator.pop(context);
                                      }
                                    }
                                  },
                                  title: 'ذخیره',
                                )
                              ],
                            )),
                        color: Colors.transparent,
                      ),
                    ],
                  ),
                  margin:
                      EdgeInsets.only(bottom: 50, left: 12, right: 12, top: 50),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(40),
                  ),
                ),
              ),
            ),
          );
        },
        transitionBuilder: (_, anim, __, child) {
          return SlideTransition(
            position:
                Tween(begin: Offset(0, -1), end: Offset(0, 0)).animate(anim),
            child: child,
          );
        },
      );
    }

    return Scaffold(
      body: user == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20, right: 30),
                      child: Row(
                        children: [
                          AddButton(
                            icon: Icons.add,
                            onPress: () async {
                              // showDialog();
                              Product product = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      ChangeNotifierProvider.value(
                                    value: ProductController(),
                                    child: AddHomeMob(userId: user.id),
                                  ),
                                ),
                              );
                              profileController.addProduct(product);
                              addToPublicList(product, rentController,
                                  salesController, user);
                            },
                            title: 'ثبت جدید',
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 100,
                          child: SimpleText(
                            text: user.name,
                            onTap: () {
                              showDialog(
                                  user.name, 'اسم', 'تغییر اسم', user, 1);
                            },
                          ),
                        ),
                        SizedBox(
                          width: 70,
                        ),
                        CircleAvatar(
                          radius: 70,
                          backgroundColor: AppColor.textColor,
                          backgroundImage: user.profileAddress != '0'
                              ? NetworkImage(
                                  MyRequest.baseUrl + user.profileAddress)
                              : null,
                          child: Stack(
                            children: [
                              user.profileAddress != '0'
                                  ? Container()
                                  : Align(
                                      alignment: Alignment.center,
                                      child: SimpleTextWhit(text: 'P'),
                                    ),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Center(
                                    child: CustomPaint(
                                      painter: Rectangle(),
                                      size: Size(400, 10),
                                    ),
                                  )),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 0),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.camera_alt,
                                      color: Colors.black38,
                                    ),
                                    onPressed: () {
                                      showCameraButton(context);
                                    },
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.2,
                      height: 1,
                      color: AppColor.textColor,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        // Expanded(
                        //   child: Center(
                        //       child: Column(
                        //     children: [
                        //       SimpleText(
                        //         text: 'درخواست ها',
                        //       ),
                        //       SizedBox(
                        //         height: 8,
                        //       ),
                        //       SimpleText(
                        //         text: NumberUtility.changeDigit(
                        //             '4', NumStrLanguage.Farsi),
                        //       )
                        //     ],
                        //   )),
                        // ),
                        Expanded(
                          child: Center(
                              child: Column(
                            children: [
                              SimpleText(
                                text: 'ثبت شده ها',
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              profileController.products == null
                                  ? Container()
                                  : profileController.products.length == 0
                                      ? Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            SimpleText(
                                              text: 'شما خونه ای ثبت نکردید',
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: SimpleText(
                                                text:
                                                    'میتونید از دکمه بالا سمت راست (ثبت جدید) برای اضافه خونه استفاده کنید.',
                                              ),
                                            ),
                                          ],
                                        )
                                      : SimpleText(
                                          text: NumberUtility.changeDigit(
                                              '${profileController.products.length}',
                                              NumStrLanguage.Farsi),
                                        )
                            ],
                          )),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: SimpleText(
                        text: user.description == '0'
                            ? 'توضیحات : ' +
                                'شما توضیحاتی ندارید میتونید با کلیک روی این متن توضیحات خودتونو بنویسید.'
                            : 'توضیحات : ' + user.description,
                        onTap: () {
                          showDialog(
                              user.description == '0' ? '' : user.description,
                              'ویرایش توضیحات',
                              'توضیحات شما',
                              user,
                              2);
                        },
                      ),
                    ),
                    profileController.products != null
                        ? MyProductList(
                            products: profileController.products, user: user,salesController: salesController,rentController: rentController,)
                        : MyLoadingProductList(),
                  ],
                ),
              ),
            ),
    );
  }
}
