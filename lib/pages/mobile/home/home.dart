import 'package:advisor/colors/colors.dart';
import 'package:advisor/pages/loading.dart';
import 'package:advisor/pages/mobile/home/profile/profile.dart';
import 'package:advisor/pages/mobile/home/rent/rent.dart';
import 'package:advisor/pages/mobile/home/sales/sales.dart';
import 'package:advisor/providers/Rentcontroller.dart';
import 'package:advisor/providers/profilecontroller.dart';
import 'package:advisor/providers/saledcontroller.dart';
import 'package:advisor/moudles/Rent.dart';
import 'package:advisor/moudles/product.dart';
import 'package:advisor/moudles/sales.dart';
import 'package:advisor/moudles/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeMobile extends StatelessWidget {
  final List<Sale> sales;
  final List<Rent> rents;

  HomeMobile({this.sales,this.rents});

  @override
  Widget build(BuildContext context) {
    List<Product> products = Provider.of<List<Product>>(context);
    User user = Provider.of<User>(context);
    ProfileController profileController = new ProfileController();
    profileController.setProducts(products);
    SalesController salesController = SalesController();
    salesController.update(sales);
    RentController rentController = RentController();
    rentController.update(rents);

    return user == null?LoadingPage(): DefaultTabController(
        length: 3,
        child: Scaffold(
          bottomNavigationBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: SafeArea(
              child: Container(
                color: AppColor.textColor,
                child: TabBar(
                  indicatorColor: Colors.white,
                  labelColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorPadding: EdgeInsets.all(5.0),
                  tabs: [
                    Tab(text: 'فروش',),
                    Tab(text: 'اجاره',),
                    Tab(text: 'پروفایل',),
                  ],
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              ChangeNotifierProvider.value(value:salesController,child: SalesMob()),
              ChangeNotifierProvider.value(value: rentController,child: RentMob(rents:rents)),
              ChangeNotifierProvider.value(
                value: profileController,
                  child: ProfileMob(user:user,rentController: rentController,salesController: salesController,)
              ),
            ],
          ),
        ),
    );
  }
}
