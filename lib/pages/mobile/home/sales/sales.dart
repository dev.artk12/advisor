import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/other.dart';
import 'package:advisor/components/sale_mob_cart.dart';
import 'package:advisor/components/searchfieldwithoutlable.dart';
import 'package:advisor/pages/mobile/mobfilters/filteractions.dart';
import 'package:advisor/pages/mobile/mobfilters/sellfilter.dart';
import 'package:advisor/providers/saledcontroller.dart';
import 'package:advisor/providers/filtercontroller.dart';
import 'package:advisor/moudles/sales.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

class SalesMob extends StatelessWidget {
  Widget searchWidgets(BuildContext context, SalesController pageController) {
    return Container(
        height: 70,
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
            color: Colors.black54,
            blurRadius: 15.0,
            offset: Offset(0.0, 0.75),
          )
        ]),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 5,
                child: Center(
                    child: SearchFieldWithOutLabel(
                  onChange: pageController.onChangeSearchText,
                  iconData: Icons.search,
                )),
              ),
              Expanded(
                  flex: 1,
                  child: Center(
                      child: IconButton(
                          onPressed: () {
                            OtherWidgets.showSortingOption(
                                context, pageController.changeSortIndex, () {
                              Navigator.pop(context);
                            }, pageController.sortIndex,
                                pageController.sortingOption);
                          },
                          icon: Icon(
                            Icons.sort,
                            color: AppColor.textColor,
                          )))),
              Expanded(
                  flex: 1,
                  child: Center(
                      child: IconButton(
                          onPressed: () async {
                            FilterController filterController =
                                new FilterController();
                            filterController
                                .updateFilter(pageController.filters);
                            Map<String, dynamic> filters = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ChangeNotifierProvider.value(
                                  value: filterController,
                                  child: BuyFilterMob(),
                                ),
                              ),
                            );
                            if (filters != null) {
                              pageController.updateFilters(filters);
                            }
                          },
                          icon: Icon(
                            MdiIcons.filter,
                            color: AppColor.textColor,
                          )))),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    SalesController pageController = Provider.of<SalesController>(context);
    List<Sale> sales = pageController.searchSales;
    FilterAction filterActions = FilterAction(filters: pageController.filters);
    sales = filterActions.filterActionsPrice(sales);
    sales = filterActions.filterActionsArea(sales);
    sales = filterActions.filterActionsBedroom(sales);
    sales = filterActions.filterActionsParking(sales);

    return SafeArea(
      child: Scaffold(
        body: ListView.builder(
          itemBuilder: (context, index) {
            return index == 0
                ? searchWidgets(context, pageController)
                : FutureProvider.value(
                    value: MyRequest.getMyProductImageLink(
                        sales[index - 1].id, sales[index - 1].status),
                    child: SaleMobCart(
                      sale: sales[index - 1],
                    ),
                  );
          },
          itemCount: sales.length + 1,
        ),
      ),
    );
  }
}
