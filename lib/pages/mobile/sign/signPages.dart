
import 'package:advisor/colors/colors.dart';
import 'package:advisor/pages/mobile/sign/sign_detail.dart';
import 'package:advisor/pages/mobile/sign/sign_phone.dart';
import 'package:advisor/pages/mobile/sign/sign_smscode.dart';
import 'package:advisor/providers/signcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SignMob extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    PageController pageController = new PageController();
    SignController signController = Provider.of<SignController>(context);

    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/phone_sign.jpg'),
                  fit: BoxFit.cover,
                )),
          ),
          Container(
            decoration:
            new BoxDecoration(color: AppColor.signColorBackgroundColor),
          ),
          PageView(
            controller: pageController,
            physics: NeverScrollableScrollPhysics(),
            children: [
              SignPhoneMob(signController: signController,pageController: pageController,),
              SignSmsCodeMob(signController: signController,pageController: pageController),
              SignDetailMob(signController: signController,pageController: pageController,),
            ],
          )
        ],
      ),
    );
  }
}
