import 'dart:convert';

import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/button.dart';
import 'package:advisor/components/errormessage.dart';
import 'package:advisor/components/field.dart';
import 'package:advisor/components/titletext.dart';
import 'package:advisor/providers/signcontroller.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class SignPhoneMob extends StatelessWidget {

  final SignController signController;
  final PageController pageController;
  SignPhoneMob({this.signController,this.pageController});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Builder(
        builder: (context)=>SafeArea(
          child: Stack(
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(child: TitleText(title: "مشاور املاک")),
                      SizedBox(
                        height: 60,
                      ),
                      Center(
                        child: Field(
                          label: 'شماره تماس',
                          iconData: Icons.phone,
                          initValue: signController.phone.isNotEmpty?signController.phone:'',
                          onChange: signController.onChangePhone,
                          textInputType: TextInputType.phone,
                          hint: '۰۹xxxxxxxxx',
                          maxText: 11,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Button(
                        title: 'ارسال پیامک',
                        onPress: () async {
                          bool check = signController.validatePhone();
                          if(!check){
                            ShowMessages.error('شماره تماس معتبر نمیباشد', context);
                          }else{
                            ShowMessages.error('لطفا صبر کنید...', context);
                            // print(NumberUtility.changeDigit(signController.phone.trim(), NumStrLanguage.English));

                            String res = await MyRequest.insert({'phone':signController.phone}, 'sendsms.php');
                            if(res!= 'not fount' && res!= '-1'){
                              Map<String ,dynamic> map = json.decode(res);

                              signController.smsCode = map['code'].toString();
                              signController.name = map['name'];
                              signController.imgLink = map['profileAddress'];
                              pageController.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                            }else{
                              ShowMessages.error('لطفا اینترنت خود را چک کنید.', context);
                            }
                          }
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
