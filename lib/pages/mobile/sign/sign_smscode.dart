import 'dart:convert';
import 'package:advisor/components/button.dart';
import 'package:advisor/components/countdown_timer.dart';
import 'package:advisor/components/errormessage.dart';
import 'package:advisor/components/field.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:advisor/providers/signcontroller.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class SignSmsCodeMob extends StatefulWidget {
  final SignController signController;
  final PageController pageController;

  SignSmsCodeMob({this.signController, this.pageController});

  @override
  State<StatefulWidget> createState() => SignSmsCodeMobState();
}

class SignSmsCodeMobState extends State<SignSmsCodeMob> {
  bool sendAgainButton = false;
  CountDownTimer countDownTimer;

  @override
  void initState() {
    super.initState();
    countDownTimer = new CountDownTimer(
      secondsRemaining: 61,
      whenTimeExpires: () {
        setState(() {
          sendAgainButton = true;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(
        child: Builder(
          builder: (context) => Stack(
            children: [
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SimpleTextWhit(text: 'ارسال پیام به '+NumberUtility.changeDigit(widget.signController.phone, NumStrLanguage.Farsi)),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SimpleTextWhit(
                            text: 'این شماره شما نیست؟',
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          GestureDetector(
                            onTap: (){
                              widget.pageController.animateToPage(0,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.easeIn);
                            },
                            child: SimpleTextWhit(
                              text: 'تغییر شماره',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Center(
                        child: Field(
                          label: 'کد پیام',
                          iconData: Icons.sms,
                          onChange: widget.signController.onChangeSmsCodeUser,
                          iconColor: Colors.white60,
                          textInputType: TextInputType.number,
                          maxText: 4,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Button(
                        title: 'ورود',
                        onPress: () async {
                          if(widget.signController.smsCodeUser == widget.signController.smsCode){
                            widget.pageController.animateToPage(2,
                                duration: Duration(milliseconds: 500),
                                curve: Curves.easeIn);
                            SystemChannels.textInput.invokeMethod('TextInput.hide');
                          }else{
                            ShowMessages.error('کد اشتباه است', context);
                          }
                          // String check = signUpController.checkingFields();
                          // if(check != 'done'){
                          //   signUpController.showMessage(check, context);
                          // }else{
                          //   if(signUpController.image == null){
                          //     signUpController.profile['image'] = '0';
                          //     signUpController.profile['imageName'] = '0';
                          //   }
                          //   String result = await MyRequest.insert(signUpController.profile, 'addUser.php');
                          //   print(result);
                          // }
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: GestureDetector(
                              onTap: (){
                                setState(() {
                                  sendAgainButton = false;
                                  countDownTimer = new CountDownTimer(
                                    secondsRemaining: 60,
                                    whenTimeExpires: () {
                                      setState(() {
                                        sendAgainButton = true;
                                      });
                                    },
                                  );
                                });

                              },
                              child: Center(
                                  child: GestureDetector(
                                    onTap: ()async{
                                      if(sendAgainButton == true){
                                        ShowMessages.error('لطفا صبر کنید...', context);
                                        String res = await MyRequest.insert({'phone':NumberUtility.changeDigit(widget.signController.phone.trim(), NumStrLanguage.English)}, 'sendsms.php');
                                        if(res!= 'not fount' && res!= '-1'){
                                          Map<String ,dynamic> map = json.decode(res);
                                          widget.signController.smsCode = map['code'].toString();
                                          widget.signController.name = map['name'];
                                          ShowMessages.error('پیامک برای شما مجدد ارسال شد.', context);
                                        }else{
                                          ShowMessages.error('لطفا اینترنت خود را چک کنید.', context);
                                        }
                                      }
                                    },
                                    child: SimpleTextWhit(
                                      color: sendAgainButton == true?Colors.white:Colors.white54,
                                text: 'ارسال مجدد',
                              ),
                                  )),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: InkWell(
                              child: countDownTimer,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
