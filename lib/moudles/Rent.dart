import 'dart:convert';

class Rent {
  final String id;
  final String status;
   String state;
   String city;
   String price;
   String area;
   String parking;
   String bedroom;
   String address;
   String description;
  final String userId;
  final String dateTime;
  final String name;
   String bathroom;
   String stateName;
   String cityName;
  final String mobile;
  final String profileAddress;

  Rent(
      {this.id, this.status, this.state, this.city, this.price, this.area, this.parking, this.stateName, this.cityName,
        this.bedroom, this.address, this.description, this.userId, this.dateTime, this.name,
        this.bathroom, this.profileAddress, this.mobile});

  Rent fromJson(Map<String, dynamic> json) {

    return new Rent(
        id: json['ID'].toString(),
        status: json['status'].toString(),
        state: json['state'].toString(),
        city: json['city'].toString(),
        price: json['price'].toString(),
        area: json['area'].toString(),
        parking: json['parking'].toString(),
        bedroom: json['bedroom'].toString(),
        address: json['address'].toString(),
        description: json['description'].toString(),
        userId: json['userId'].toString(),
        dateTime: json['datetime'].toString(),
        name: json['name'].toString(),
        bathroom: json['bathroom'].toString(),
        cityName: json['cityname'],
        stateName: json['statename'],
        mobile: json['mobile'],
        profileAddress: json['profileaddress'],);
  }

  List<Rent> getRentListFromJson(String body) {
    return (json.decode(body) as List).map((e) => fromJson(e)).toList();
  }
}
