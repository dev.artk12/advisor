
import 'dart:convert';

class Sale {

  final String id;
  final String status;
   String state;
   String city;
   String stateName;
   String cityName;
   String price;
   String area;
   String parking;
   String bedroom;
   String address;
   String description;
  final String userId;
  final String dateTime;
  final String name;
   String bathroom;
  final String mobile;
  final String profileAddress;

  Sale({this.id,this.status,this.state,this.city,this.price,this.area,this.parking,this.cityName,this.stateName,
  this.bedroom,this.address,this.description,this.userId,this.dateTime,this.name,this.bathroom,this.profileAddress,this.mobile});

  Sale fromJson(Map<String,dynamic> json){
    // print(json['userId']);
    return new Sale(
      id:json['ID'].toString(),
      status: json['status'].toString(),
      state: json['state'].toString(),
      city: json['city'].toString(),
      price: json['price'].toString(),
      area: json['area'].toString(),
      parking: json['parking'].toString(),
      bedroom: json['bedroom'].toString(),
      address: json['address'].toString(),
      description: json['description'].toString(),
      dateTime: json['datetime'].toString(),
      userId: json['userId'].toString(),
      name: json['name'].toString(),
      bathroom: json['bathroom'].toString(),
      cityName: json['cityname'],
      stateName: json['statename'],
      mobile: json['mobile'],
      profileAddress: json['profileaddress'],

    );
  }

  List<Sale> getSaleListFromJson(String body){
    return (json.decode(body) as List).map((e) => fromJson(e)).toList();
  }

}