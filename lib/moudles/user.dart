
import 'dart:convert';

class User{

  final String id;
  String name;
  String profileAddress;
  final String mobileNumber;
  String description;

  User({this.id,this.name,this.profileAddress,this.mobileNumber,this.description});

  User fromJson(Map<String, dynamic> map){
    return User(
      id: map['ID'].toString(),
      name: map['name'],
      mobileNumber: map['mobilenumber'],
      profileAddress: map['profileaddress'],
      description: map['description']
    );
  }

  User getUserAccount(String body){
    // print(body);
    return (json.decode(body) as List).map((e) => fromJson(e)).toList()[0];
  }

}