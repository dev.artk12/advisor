
import 'dart:convert';

class ProductImageLink {
  final String id;
  final String productId;
  final String link;

  ProductImageLink({this.productId,this.id,this.link});

  ProductImageLink fromJson(Map<String , dynamic> map){
    return ProductImageLink(
      id: map['ID'],
      productId: map['productid'],
      link: map['link']
    );
  }

  List<ProductImageLink> getProductListFromJson(String body){
    return (json.decode(body) as List).map((e) => fromJson(e)).toList();
  }
}