
import 'dart:convert';

class Product {

  final String id;
  final String status;
   String state;
   String city;
   String price;
   String area;
   String parking;
   String bedroom;
   String address;
   String description;
  final String userId;
   String bathroom;
   String cityName;
   String stateName;
  final String dateTime;

  Product({this.id,this.status,this.state,this.city,this.price,this.area,this.parking,this.dateTime,
  this.bedroom,this.address,this.description,this.userId,this.bathroom,this.cityName,this.stateName});

  Product fromJson(Map<String,dynamic> json){
    // print(json['userId']);
    return new Product(
      id:json['ID'].toString(),
      status: json['status'].toString(),
      state: json['state'].toString(),
      city: json['city'].toString(),
      price: json['price'].toString(),
      area: json['area'].toString(),
      parking: json['parking'].toString(),
      bedroom: json['bedroom'].toString(),
      address: json['address'].toString(),
      description: json['description'].toString(),
      userId: json['userId'].toString(),
      bathroom: json['bathroom'].toString(),
    );
  }

  List<Product> getProductListFromJson(String body){
    return (json.decode(body) as List).map((e) => fromJson(e)).toList();
  }

}