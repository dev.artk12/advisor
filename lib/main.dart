import 'package:advisor/requests/reqests.dart';
import 'package:advisor/requests/sharedprefrences.dart';
import 'package:advisor/requests/sqlite/sqlService.dart';
import 'package:advisor/pages/wrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'moudles/Rent.dart';
import 'moudles/sales.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SqLiteService.createDBIfUserExists();
    return MaterialApp(
      title: 'Home',
      debugShowCheckedModeBanner: false,
      builder: (BuildContext context, Widget child) {
        return new Directionality(
          textDirection: TextDirection.rtl,
          child: new Builder(
            builder: (BuildContext context) {
              return new MediaQuery(
                data: MediaQuery.of(context).copyWith(
                  textScaleFactor: 1.0,
                ),
                child: child,
              );
            },
          ),
        );
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiProvider(
        providers: [
          FutureProvider<List<Sale>>(
            create: (_) => MyRequest.getSalesProducts(),
          ),
          FutureProvider<List<Rent>>(
            create: (_) => MyRequest.getRentProducts(),
          ),
          FutureProvider<bool>(
            create: (_) => MySharedPreferences.getSign(),
          ),
        ],
        child: Wrapper(),
      ),
    );
  }
}
