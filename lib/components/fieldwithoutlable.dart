
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FieldWithOutLabel extends StatelessWidget {

  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;

  FieldWithOutLabel({this.iconData,this.onChange,this.obscureText = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      child: TextField(
        onChanged: onChange,
        obscureText: obscureText,
        decoration: InputDecoration(
          icon: iconData != null ? Icon(iconData,color: Colors.white):null,
          focusColor: Colors.white,
          focusedBorder:UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ) ,
      ),
    );
  }
}
