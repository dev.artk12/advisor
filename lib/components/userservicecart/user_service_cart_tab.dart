import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/moudles/user.dart';
import 'package:advisor/pages/mobile/home/profile/myproduct/edit.dart';
import 'package:advisor/pages/tablet/home/profile/myproduct/edit.dart';
import 'package:advisor/providers/Rentcontroller.dart';
import 'package:advisor/providers/productpagecontroller.dart';
import 'package:advisor/providers/profilecontroller.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:advisor/providers/saledcontroller.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:provider/provider.dart';
import '../simpletextbiggerfont.dart';
import '../simpletextprice.dart';
import '../simpletextwhitbiggerfont.dart';

class UserServiceCartTab extends StatelessWidget {
  final String id;
  final String status;
  final String statusId;
  final String area;
  final String bedRooms;
  final String parking;
  final String price;
  final String state;
  final String city;
  final String address;
  final String description;
  final ProfileController profileController;
  final String bathroom;
  final User user;
  final SalesController salesController;
  final RentController rentController;

  UserServiceCartTab(
      {@required this.status,
      @required this.area,
      this.profileController,
      this.rentController,
      this.salesController,
      @required this.bedRooms,
      @required this.parking,
      @required this.price,
      @required this.id,
      @required this.user,
      @required this.state,
      @required this.city,
      @required this.address,
      @required this.description,
      @required this.statusId,
      @required this.bathroom});

  @override
  Widget build(BuildContext context) {
    List<ProductImageLink> productImageLinks =
        Provider.of<List<ProductImageLink>>(context) ?? [];

    return Card(
      margin: EdgeInsets.all(5),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: InkWell(
        onTap: () {
          ProductController productController = new ProductController();
          productController.update(
              id,
              statusId,
              state,
              city,
              price,
              parking,
              bedRooms,
              area,
              address,
              description,
              productImageLinks,
              bathroom);

          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ChangeNotifierProvider(
                  create: (_) => productController,
                  child: EditHomeTab(
                    profileController: profileController,
                    user: user,
                    rentController: rentController,
                    salesController: salesController,
                  ))));
        },
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(
                color: AppColor.textColor,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(5), topLeft: Radius.circular(5)),
              ),
              child: Center(
                child: SimpleTextWhitBiggerFont(text: status),
              ),
            ),
            Container(
              height: 150,
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(5),
                            bottomLeft: Radius.circular(5)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: productImageLinks.length != 0
                              ? NetworkImage(
                                  MyRequest.baseUrl + productImageLinks[0].link)
                              : AssetImage('assets/images/black.jpg'),
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          width: MediaQuery.of(context).size.width / 2,
                          child: Center(
                            child: Column(
                              children: [
                                // Container(padding: EdgeInsets.only(top:0,bottom: 10),child: SimpleTextBiggerFont(text:status)),
                                Padding(
                                  padding: const EdgeInsets.only(right: 4.0),
                                  child: Row(
                                    children: [
                                      SimpleText(text: ' قیمت : '),
                                      SimpleTextPrice(
                                          text: NumberUtility.changeDigit(
                                                  price, NumStrLanguage.Farsi) +
                                              ' تومان'),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Container(
                                  height: 2,
                                  color: Colors.black12,
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 4.0),
                                  child: Row(
                                    children: [
                                      SimpleText(text: ' متراژ : '),
                                      SimpleTextPrice(
                                          text: NumberUtility.changeDigit(
                                                  area, NumStrLanguage.Farsi) +
                                              ' متر'),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Container(
                                  height: 2,
                                  color: Colors.black12,
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 4.0),
                                  child: Row(
                                    children: [
                                      SimpleText(text: ' پارکینگ : '),
                                      int.parse(parking) > 0
                                          ? SimpleTextBiggerFont(text: ' دارد ')
                                          : SimpleTextBiggerFont(
                                              text: ' ندارد ')
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Container(
                                  height: 2,
                                  color: Colors.black12,
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 4.0),
                                  child: Row(
                                    children: [
                                      SimpleText(text: ' تعداد اتاق خوابها : '),
                                      SimpleTextBiggerFont(
                                          text: NumberUtility.changeDigit(
                                              bedRooms, NumStrLanguage.Farsi)),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Container(
                                  height: 2,
                                  color: Colors.black12,
                                ),
                                SizedBox(
                                  height: 4,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 4.0),
                                  child: Row(
                                    children: [
                                      SimpleText(
                                          text: ' تعداد سرویس بهداشتی : '),
                                      SimpleTextBiggerFont(
                                          text: NumberUtility.changeDigit(
                                              bathroom, NumStrLanguage.Farsi))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
