import 'package:advisor/components/simpletext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class UserServiceCartLoading extends StatefulWidget {


  createState() =>UserServiceCartLoadingSate();
}
class UserServiceCartLoadingSate extends State<UserServiceCartLoading> with SingleTickerProviderStateMixin{

  AnimationController controllerOne;
  Animation<Color> animationOne;
  Animation<Color> animationTwo;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controllerOne = AnimationController(
        duration: Duration(milliseconds: 3000),
        vsync: this);
    animationOne = ColorTween(begin: Colors.grey,end: Colors.white70).animate(controllerOne);
    animationTwo = ColorTween(begin: Colors.white70,end: Colors.grey).animate(controllerOne);
    controllerOne.forward();
    controllerOne.addListener((){
      if(controllerOne.status == AnimationStatus.completed){
        controllerOne.reverse();
      } else if(controllerOne.status == AnimationStatus.dismissed){
        controllerOne.forward();
      }
      this.setState((){});
    });
  }

  @override
  void dispose() {
    controllerOne.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: ShaderMask(
        shaderCallback: (rect){
          return LinearGradient(
              tileMode: TileMode.mirror,
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [animationOne.value,animationTwo.value]).createShader(rect,textDirection: TextDirection.rtl);
        },
        child: Container(
          height: 140,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(width: MediaQuery.of(context).size.width /14,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // SimpleText(text:'اجاره'),
                  Container(width: 40, height: 10, color: Colors.white,),
                  SizedBox(height: 185/7,),
                  Container(width: 40, height: 10, color: Colors.white,),
                  // SimpleText(text:NumberUtility.changeDigit('50000', NumStrLanguage.Farsi)),
                  SizedBox(height: 185/7,),
                  Container(width: 40, height: 10, color: Colors.white,),
                  // SimpleText(text:NumberUtility.changeDigit('3 خواب', NumStrLanguage.Farsi)),
                ],
              ),
              SizedBox(width: MediaQuery.of(context).size.width /5,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // SimpleText(text:NumberUtility.changeDigit('2 پارکینگ', NumStrLanguage.Farsi)),
                  Container(width: 40, height: 10, color: Colors.white,),
                  SizedBox(height: 185/7,),
                  // SimpleText(text:NumberUtility.changeDigit('700 متر', NumStrLanguage.Farsi)),
                  Container(width: 40, height: 10, color: Colors.white,),
                ],
              ),
              Expanded(child:Text('')),
              Container(
                height: 140,
                width: MediaQuery.of(context).size.width/3,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: Colors.transparent,
                  ),
                  // image: DecorationImage(
                  //   fit: BoxFit.cover,
                  //   image: AssetImage('assets/images/phone_sign.jpg'),
                  // ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
