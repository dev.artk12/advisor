
import 'package:advisor/colors/colors.dart';
import 'package:advisor/moudles/numberconverter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SimpleTextBiggerFont extends StatelessWidget {
  final String text;
  SimpleTextBiggerFont({this.text});

  @override
  Widget build(BuildContext context) {

    return   Text("$text",
      style: TextStyle(fontFamily: !NumberConverter.checkEnglish(text)?"iranian_sans":null,
          color: AppColor.textColor,fontSize: 15,fontWeight: FontWeight.bold),);
  }
}
