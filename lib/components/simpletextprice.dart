import 'package:advisor/colors/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleTextPrice extends StatelessWidget {
  final String text;
  final Function onTap;
  SimpleTextPrice({this.text,this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Text("$text",
          style: TextStyle(fontSize: text.length > 13 ?12:15,fontWeight: FontWeight.bold,fontFamily: "iranian_sans", color: AppColor.textColor)),
    );
  }
}
