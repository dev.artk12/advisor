
import 'package:advisor/colors/colors.dart';
import 'package:advisor/moudles/numberconverter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SimpleTextWhitBiggerFont extends StatelessWidget {
  final String text;
  SimpleTextWhitBiggerFont({this.text});

  @override
  Widget build(BuildContext context) {

    return   Text("$text",
      style: TextStyle(fontFamily: !NumberConverter.checkEnglish(text)?"iranian_sans":null,
          color: Colors.white,fontSize: 15,fontWeight: FontWeight.bold),);
  }
}
