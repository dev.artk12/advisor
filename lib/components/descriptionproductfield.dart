

import 'package:advisor/colors/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DescriptionProductField extends StatelessWidget {
  final String label;
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;
  final String initText;

  DescriptionProductField({this.label,this.iconData,this.onChange,this.obscureText = false,this.initText});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      child: TextField(
        maxLines: 5,
        cursorColor: AppColor.textColor,
        onChanged: onChange,
        obscureText: obscureText,
        style: TextStyle(
            fontSize: 12,
            color: AppColor.buttonBackgroundColor,
            fontFamily: "iranian_sans",
            fontWeight: FontWeight.w300
        ),
        decoration: InputDecoration(
          labelText: label,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: AppColor.textColor),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: AppColor.textColor),
          ),
          icon: iconData != null ? Icon(iconData,color: Colors.white):null,
          focusColor: Colors.white,
          labelStyle: TextStyle(
              fontSize: 12,
              color: AppColor.buttonBackgroundColor,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ) ,
      ),
    );
  }
}
