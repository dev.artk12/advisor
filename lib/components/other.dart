
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/components/add_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'button.dart';

class OtherWidgets{

  static void showSortingOption(BuildContext context, Function(int) onChange,Function onPress,int sortIndex ,List<String> list) {
    print(sortIndex);
    int i = sortIndex;
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) =>
                Directionality(
                  textDirection: TextDirection.rtl,
                  child: Container(
                    height: 120,
                    color: Color(0xFF737373),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16.0),
                              topRight: Radius.circular(16.0))),
                      // height: 100,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: ListView(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SimpleText(text: 'مرتب بر اساس : '),
                                DropdownButton(
                                    value: i,
                                    items: CreateListDropDown()
                                        .dropDownList(list),
                                    onChanged: (int index) {
                                      onChange(index);
                                      setState(() {
                                        i = index;
                                      });
                                    }),
                              ],
                            ),
                            Center(
                                child: Container(
                                    width: 100,
                                    child: Button(
                                      title: 'تمام',
                                      onPress: onPress
                                    ))),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
          );
        });
  }

  static Widget divider(String text){
    return Container(
      height: 30,
      child: Stack(
        children: [
          Align(alignment: Alignment.center,child: Container(height: 1,color: Colors.black26,)),
          Align(alignment: Alignment.centerRight,child: Container(
            margin: const EdgeInsets.only(bottom: 5,right: 30,left: 10),
            child: Container(padding: EdgeInsets.only(right: 5,left: 5),color: Colors.white,child: SimpleText(text:text)),
          )),
        ],
      ),
    );
  }

  static Widget dividerWithCenterText(String text){
    return Container(
      height: 30,
      child: Stack(
        children: [
          Align(alignment: Alignment.center,child: Container(height: 1,color: Colors.black26,)),
          Align(alignment: Alignment.center,child: Container(
            margin: const EdgeInsets.only(bottom: 5),
            child: Container(padding: EdgeInsets.only(right: 5,left: 5),color: Colors.white,child: SimpleText(text:text)),
          )),
        ],
      ),
    );
  }

}