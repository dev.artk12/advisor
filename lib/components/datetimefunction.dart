
import 'package:shamsi_date/shamsi_date.dart';

class DateTimeFunction {

  static List<String> month = [
    'فروردین',
    'اردیبهشت',
    'خرداد',
    'تیر',
    'مرداد',
    'شهریور',
    'مهر',
    'آبان',
    'آذر',
    'دی',
    'بهمن',
    'اسفند',
  ];

  static Jalali stringDateTimeToJalali(String date){
    String year = date.substring(0, 4).replaceAll('/', '');
    String month = date
        .substring(date.indexOf('/'), date.lastIndexOf('/'))
        .replaceAll('/', '');
    String day = date
        .substring(date.lastIndexOf('/'), date.length)
        .replaceAll('/', '');

    return Gregorian(int.parse(year),int.parse(month),int.parse(day)).toJalali();
  }

  static String getDateFormat(String date){
    Jalali j = stringDateTimeToJalali(date);
    return '${j.day} ${month[j.month-1]} ${j.year}';
  }
}