
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'currencyarformatter.dart';

class Field extends StatelessWidget {

  final String label;
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;
  final TextInputType textInputType;
  final Color iconColor;
  final String initValue;
  final String hint;
  final int maxText;

  Field({this.maxText,this.hint,this.initValue,this.label,this.iconData,this.onChange,this.obscureText = false,this.textInputType = TextInputType.text,this.iconColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      child: TextFormField(
        keyboardType: textInputType,
        cursorColor: Colors.white30,
        onChanged: onChange,
        initialValue: initValue,
        obscureText: obscureText,
        maxLength: maxText,
        textAlign: TextAlign.center,
        inputFormatters: [
          CurrencyArInputFormatter(),
        ],
        style: TextStyle(
            color: Colors.white,
        ),
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(
            fontSize: 12,
            color: Colors.white54,
            fontFamily: "iranian_sans",
            fontWeight: FontWeight.w300
        ),
          labelText: label,
          icon: iconData != null ? Icon(iconData,color: iconColor):null,
          focusColor: Colors.white,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white30),
          ),
          focusedBorder:UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          labelStyle: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ) ,
      ),
    );
  }
}
