import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/datetimefunction.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/moudles/Rent.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:advisor/pages/mobile/productpage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:provider/provider.dart';

import '../requests/reqests.dart';

class RentMobCart extends StatelessWidget {
  final Rent rent;
  RentMobCart({this.rent});
  @override
  Widget build(BuildContext context) {

    List<ProductImageLink> productImages = Provider.of<List<ProductImageLink>>(context)??[];
    return GestureDetector(
      onTap: (){
        // print();
        Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductPage(
          bathroom: rent.bathroom,
          description: rent.description,
          status: rent.status,
          price: rent.price,
          area: rent.area,
          bedroom: rent.bedroom,
          imageLinks: productImages,
          mobile: rent.mobile,
          name: rent.name,
          profileAddress: rent.profileAddress,
        )));
      },
      child: Card(
        margin: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 185,
                    child: Align(
                      alignment: AlignmentDirectional.bottomEnd,
                      child: ClipRRect(
                        child: Container(
                          margin: EdgeInsets.all(10),
                          padding:
                              EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                          child: Text(NumberUtility.changeDigit(DateTimeFunction.getDateFormat(rent.dateTime), NumStrLanguage.Farsi),
                              style: TextStyle(color: Colors.white)),
                          decoration: BoxDecoration(
                            color: AppColor.signColorBackgroundColor,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: Colors.transparent,
                            ),
                          ),
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        color: Colors.transparent,
                      ),
                      image: DecorationImage(
                        fit: productImages.length == 0? BoxFit.contain:BoxFit.cover,
                        // image: AssetImage('assets/images/loading.gif'),
                        image: productImages.length == 0? AssetImage('assets/images/loading.gif')
                            :NetworkImage(MyRequest.baseUrl+productImages[0].link),
                      ),
                    ),
                  ),

                  Container(
                    child: Column(
                      children: [
                        SizedBox(height: 185-(100/2),),
                        Container(
                          margin: EdgeInsets.only(right: 20),
                          child: CircleAvatar(
                            radius: 40,
                            backgroundColor: AppColor.textColor,
                            backgroundImage: rent.profileAddress == '0'?null:NetworkImage(MyRequest.baseUrl+rent.profileAddress),
                            child: rent.profileAddress != '0'?null:Center(
                              child: Center(child: Icon(Icons.person,color: Colors.white,),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(

                    child: Column(
                      children: [
                        SizedBox(height: 195,),
                        Container(
                          margin: EdgeInsets.only(right: 95,left: 20),
                          child: Row(
                            children: [
                              SimpleText(text: rent.name,),
                              Expanded(child: Text(''),),
                              Row(
                                children: [
                                  SimpleText(text:  NumberUtility.changeDigit(' ت'+rent.price, NumStrLanguage.Farsi),),
                                  SimpleText(text:' / ماه'),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10,right: 10),
                          child: Stack(
                            children: [
                              Align(alignment: Alignment.topRight ,child: Icon(Icons.location_on,size: 16,color: AppColor.textColor,)),
                              Container(margin: EdgeInsets.only(right: 20),
                                  child: SimpleText(text:'استان ${rent.stateName} شهر ${rent.cityName} ${rent.address}')),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Row(
                            children: [
                              Expanded(
                                child: Center(
                                  child:Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SimpleText(text:NumberUtility.changeDigit(rent.parking == '0'?'ندارد':'دارد', NumStrLanguage.Farsi)),
                                    SizedBox(width: 5,),
                                    Icon(Icons.time_to_leave,size: 18,color: AppColor.textColor,),
                                  ],
                                ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                    child:Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        SimpleText(text:NumberUtility.changeDigit(rent.bathroom, NumStrLanguage.Farsi)),
                                        SizedBox(width: 5,),
                                        Icon(Icons.hot_tub,size: 18,color: AppColor.textColor,),
                                      ],
                                    ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                  child:
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SimpleText(text:NumberUtility.changeDigit(rent.bedroom, NumStrLanguage.Farsi)),
                                    SizedBox(width: 5,),
                                    Icon(Icons.local_hotel,size: 18,color: AppColor.textColor,),
                                  ],
                                ),),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
