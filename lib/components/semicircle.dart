
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class SemiCircle extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = Colors.white60;
    canvas.drawArc(
      Rect.fromCenter(
        center: Offset( size.width / 2,size.height / 2.5),
        height: size.height/1.5,
        width: size.width,
      ),
      math.pi,
      math.pi,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class Rectangle extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint1 = Paint()
      ..color = Colors.white60
      ..style = PaintingStyle.fill;
    canvas.drawRect(Offset(size.height/2, size.width / 5) & const Size(130, 50), paint1);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
