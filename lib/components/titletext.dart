import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TitleText extends StatelessWidget {
  final String title;
  TitleText({this.title});
  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontSize: 24,
        color: Colors.white,
        fontFamily: "iranian_sans",
          fontWeight: FontWeight.w300
      ),
    );
  }
}
