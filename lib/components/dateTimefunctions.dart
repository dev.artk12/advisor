
class DateTimeFunctions{


  static DateTime stringToDateTime(String date){
    String year = date.substring(0,4);
    String month = date.substring(date.indexOf('/')+1,date.lastIndexOf('/'));
    String day = date.substring(date.lastIndexOf('/')+1);
    return DateTime(int.parse(year),int.parse(month),int.parse(day));
  }
}