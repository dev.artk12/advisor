import 'package:advisor/colors/colors.dart';
import 'package:advisor/moudles/numberconverter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchFieldWithOutLabel extends StatelessWidget {
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;

  SearchFieldWithOutLabel(
      {this.iconData, this.onChange, this.obscureText = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      child: TextField(
        onChanged: onChange,
        obscureText: obscureText,
        style: TextStyle(
          color: Colors.black87,
          // fontFamily: !NumberConverter.checkEnglish(text)?"iranian_sans":null,
        ),
        cursorColor: Colors.black87,
        decoration: InputDecoration(
          hintText: 'جستجو',
          hintStyle: TextStyle(
              fontSize: 12,
              color: Colors.black45,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300),
          prefixIcon: iconData != null
              ? Icon(iconData, color: AppColor.textColor)
              : null,
          focusColor: Colors.black87,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide(color: Colors.black45, width: 0.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: BorderSide(color: Colors.black87, width: 0.0),
          ),
          labelStyle: TextStyle(
              fontSize: 12,
              color: Colors.black87,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300),
        ),
      ),
    );
  }
}
