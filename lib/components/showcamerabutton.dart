
import 'dart:io';

import 'package:advisor/components/simpletext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ShowCameraButtons{

  static showBottomSheetCameraButton(BuildContext context ,Function(File) whenUpdate){
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Directionality(
          textDirection: TextDirection.rtl,
          child: Container(
            height: 120,
            color: Color(0xFF737373),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16.0))),
              // height: 100,
              child: ListView(
                children: [
                  ListTile(
                    title: SimpleText(text: 'دوربین'),
                    leading: Icon(Icons.camera),
                    onTap: () async {
                      final pickedFile = await ImagePicker()
                          .getImage(source: ImageSource.camera);
                      whenUpdate(File(pickedFile.path));
                    },
                  ),
                  ListTile(
                    title: SimpleText(text: 'گالری'),
                    leading: Icon(Icons.image),
                    onTap: () async {
                      final pickedFile = await ImagePicker()
                          .getImage(source: ImageSource.gallery);
                      whenUpdate(File(pickedFile.path));
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

}