
import 'package:flutter/services.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class CurrencyArInputFormatter extends TextInputFormatter {

  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {

    String newText = NumberUtility.changeDigit(newValue.text, NumStrLanguage.Farsi);
    //
    // if(oldValue.text.length < newValue.text.length) {
    //   if (newText.replaceAll('،', '').length % 3 == 0) {
    //     newText += '،';
    //   }
    // }
    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}