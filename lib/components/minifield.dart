

import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/currencyarformatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class MiniField extends StatelessWidget {
  final String label;
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;
  final TextInputType textInputType;
  final String initText;
  final TextAlign textAlign;
  final TextEditingController editingController;
  final int maxLength;

  MiniField({this.label,this.iconData,this.onChange,this.maxLength,
    this.obscureText = false,this.textInputType = TextInputType.text,this.initText,this.textAlign = TextAlign.left,this.editingController});

  @override
  Widget build(BuildContext context) {

    return Container(
      width: MediaQuery.of(context).size.width / 2,
      child: TextFormField(
        keyboardType: textInputType,
        cursorColor: AppColor.textColor,
        textAlign: textAlign,
        controller: editingController,
        initialValue: editingController == null?initText:null,
        onChanged: editingController == null?onChange:null,
        obscureText: obscureText,
        maxLength: maxLength,
        inputFormatters: [
          CurrencyArInputFormatter(),
        ],
        style: TextStyle(
            fontSize: 12,
            color: AppColor.buttonBackgroundColor,
            fontFamily: "iranian_sans",
            fontWeight: FontWeight.w300
        ),
        decoration: InputDecoration(
          labelText: label,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: AppColor.textColor),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: AppColor.textColor),
          ),
          icon: iconData != null ? Icon(iconData,color: Colors.white):null,
          focusColor: Colors.white,
          labelStyle: TextStyle(
              fontSize: 12,
              color: AppColor.buttonBackgroundColor,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ) ,
      ),
    );
  }
}
