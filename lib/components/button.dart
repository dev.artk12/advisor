
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final String title;
  final Function onPress;
  final IconData icon;
  final Color backgroundColor;
  final Color textColor;
  Button({this.title,this.onPress,this.icon,this.backgroundColor,this.textColor});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      textColor: Colors.white,
      color: backgroundColor == null?AppColor.buttonBackgroundColor:backgroundColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),),
      child: icon != null?Container(
        width: 90,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon,color: Colors.white,),
            SimpleTextWhit(text:title,color: textColor != null?textColor:null,),
          ],
        ),
      ):SimpleTextWhit(text:title,color: textColor != null?textColor:null,),
      onPressed: onPress,
    );
  }
}
