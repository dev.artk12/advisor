

import 'package:advisor/colors/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddressField extends StatelessWidget {
  final String label;
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;
  final String initText;

  AddressField({this.label,this.iconData,this.onChange,this.obscureText = false,this.initText});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      child: TextFormField(
        maxLines: 3,
        cursorColor: AppColor.textColor,
        initialValue: initText,
        onChanged: onChange,
        obscureText: obscureText,
        decoration: InputDecoration(
          labelText: label,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: AppColor.textColor),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 1,color: AppColor.textColor),
          ),
          icon: iconData != null ? Icon(iconData,color: Colors.white):null,
          focusColor: Colors.white,
          labelStyle: TextStyle(
              fontSize: 12,
              color: AppColor.buttonBackgroundColor,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ) ,
      ),
    );
  }
}
