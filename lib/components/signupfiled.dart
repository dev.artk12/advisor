
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/currencyarformatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpField extends StatelessWidget {

  final String label;
  final IconData iconData;
  final Function(String) onChange;
  final bool obscureText;

  SignUpField({this.label,this.iconData,this.onChange,this.obscureText = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.3,
      child: TextField(
        cursorColor: Colors.black54,
        onChanged: onChange,
        obscureText: obscureText,
        textAlign: TextAlign.center,
        inputFormatters: [
          CurrencyArInputFormatter(),
        ],
        style: TextStyle(
            color: AppColor.textColor,
        ),
        decoration: InputDecoration(
          labelText: label,
          icon: iconData != null ? Icon(iconData,color: AppColor.textColor):null,
          focusColor: AppColor.textColor,
          focusedBorder:UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black87),
          ),
          labelStyle: TextStyle(
              fontSize: 12,
              color: Colors.black87,
              fontFamily: "iranian_sans",
              fontWeight: FontWeight.w300
          ),
        ) ,
      ),
    );
  }
}
