
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterButton extends StatelessWidget {
  final String title;
  final Function onPress;
  final IconData icon;
  RegisterButton({this.title,this.onPress,this.icon});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      textColor: Colors.white,
      color: Colors.transparent,
      child: icon != null?Container(
        child: Row(
          children: [
            Icon(icon,color: Colors.white,),
            SimpleTextWhit(text:title),
          ],
        ),
      ):SimpleTextWhit(text:title),
      onPressed: onPress,
    );
  }
}
