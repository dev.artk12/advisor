import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/simpletext.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:advisor/moudles/sales.dart';
import 'package:advisor/pages/mobile/productpage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:provider/provider.dart';

import '../requests/reqests.dart';
import 'datetimefunction.dart';


class SaleMobCart extends StatelessWidget {
  final Sale sale;
  SaleMobCart({this.sale});

  @override
  Widget build(BuildContext context) {
    List<ProductImageLink> productImages = Provider.of<List<ProductImageLink>>(context)??[];

    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductPage(
          bathroom: sale.bathroom,
          description: sale.description,
          status: sale.status,
          price: sale.price,
          area: sale.area,
          bedroom: sale.bedroom,
          imageLinks: productImages,
          mobile: sale.mobile,
          name: sale.name,
          profileAddress: sale.profileAddress,
        )));
      },
      child: Card(
        margin: EdgeInsets.all(5),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 185,
                    child: Align(
                      alignment: AlignmentDirectional.bottomEnd,
                      child: ClipRRect(
                        child: Container(
                          margin: EdgeInsets.all(10),
                          padding:
                              EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                          child: Text(NumberUtility.changeDigit(DateTimeFunction.getDateFormat(sale.dateTime), NumStrLanguage.Farsi),
                              style: TextStyle(color: Colors.white)),
                          decoration: BoxDecoration(
                            color: AppColor.signColorBackgroundColor,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: Colors.transparent,
                            ),
                          ),
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(
                        color: Colors.transparent,
                      ),
                      image: DecorationImage(
                        fit: productImages.length == 0? BoxFit.contain:BoxFit.cover,
                        // image: AssetImage('assets/images/loading.gif'),
                        image: productImages.length == 0? AssetImage('assets/images/loading.gif')
                            :NetworkImage(MyRequest.baseUrl+productImages[0].link),
                      ),
                    ),
                  ),

                  Container(
                    child: Column(
                      children: [
                        SizedBox(height: 185-(100/2),),
                        Container(
                          margin: EdgeInsets.only(right: 20),
                          child: CircleAvatar(
                              radius: 40,
                              backgroundColor: AppColor.textColor,
                              backgroundImage: sale.profileAddress == '0'?null:NetworkImage(MyRequest.baseUrl+sale.profileAddress),
                              child: sale.profileAddress != '0'?null:Center(
                                child: Center(child: Icon(Icons.person,color: Colors.white,),),
                              ),
                            // child: Icon(Icons.account_circle,color: Colors.white,),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        SizedBox(height: 195,),
                        Container(
                          margin: EdgeInsets.only(right: 95,left: 20),
                          child: Row(
                            children: [
                              SimpleText(text:sale.name),
                               // Text(,style: TextStyle(fontFamily: "iranian_sans",color: AppColor.textColor),),
                              Expanded(child: Text(''),),
                              Row(
                                children: [
                                  SimpleText(text:  NumberUtility.changeDigit(' ت'+sale.price, NumStrLanguage.Farsi),),
                                  SimpleText(text:' / '),
                                  SimpleText(text:NumberUtility.changeDigit(sale.area, NumStrLanguage.Farsi),),
                                  SimpleText(text:' متر '),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10,right: 10),
                          child: Stack(
                            children: [
                              Align(alignment: Alignment.topRight ,child: Icon(Icons.location_on,size: 16,color: AppColor.textColor,)),
                              Container(margin: EdgeInsets.only(right: 20),
                                  child: SimpleText(text:'استان ${sale.stateName} شهر ${sale.cityName} ${sale.address}')),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Row(
                            children: [
                              Expanded(
                                child: Center(
                                  child:Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SimpleText(text:NumberUtility.changeDigit(sale.parking == '0'?'ندارد':'دارد', NumStrLanguage.Farsi)),
                                    SizedBox(width: 5,),
                                    Icon(Icons.time_to_leave,size: 18,color: AppColor.textColor,),
                                  ],
                                ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                    child:Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        SimpleText(text:NumberUtility.changeDigit(sale.bathroom, NumStrLanguage.Farsi)),
                                        SizedBox(width: 5,),
                                        Icon(Icons.hot_tub,size: 18,color: AppColor.textColor,),
                                      ],
                                    ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                  child:
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SimpleText(text:NumberUtility.changeDigit(sale.bedroom, NumStrLanguage.Farsi)),
                                    SizedBox(width: 5,),
                                    Icon(Icons.local_hotel,size: 18,color: AppColor.textColor,),
                                  ],
                                ),),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
