import 'package:advisor/colors/colors.dart';
import 'package:advisor/moudles/numberconverter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleText extends StatelessWidget {
  final String text;
  final Function onTap;
  SimpleText({this.text,this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Text("$text",
          style: TextStyle(fontFamily: !NumberConverter.checkEnglish(text)?"iranian_sans":null, color: AppColor.textColor)),
    );
  }
}
