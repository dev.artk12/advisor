
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ShowMessages{

  static void error(String message,BuildContext context){
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message),));
  }

  static void loading(String message,BuildContext context){
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message),));
  }
}