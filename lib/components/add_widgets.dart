
import 'dart:convert';

import 'package:advisor/components/simpletext.dart';
import 'package:advisor/components/errormessage.dart';
import 'package:advisor/moudles/imageid.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:advisor/pages/mobile/dropdownholder.dart';
import 'package:advisor/requests/reqests.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CreateListDropDown {
  List<DropDownItemHolder> createList(List<String> list) {
    List<DropDownItemHolder> holderList = new List();
    for (int i = 0; i < list.length; i++) {
      holderList.add(DropDownItemHolder(val: i + 1, name: list[i]));
    }
    return holderList;
  }

  List<DropdownMenuItem<int>> dropDownList(List<String> names) {
    List<DropDownItemHolder> list = createList(names);
    List<DropdownMenuItem<int>> dropDownItem = new List();
    for (int i = 0; i < list.length; i++) {
      dropDownItem.add(DropdownMenuItem(
        child: SimpleText(text: list[i].name),
        value: list[i].val,
      ));
    }
    return dropDownItem;
  }
}

class AddProduct {
  void showAddErrorMessage(String result, BuildContext context) {
    switch (result) {
      case 'price':
        ShowMessages.error('شما هیچ قیمتی برای قطعه نزاشتین.', context);
        break;
      case 'area':
        ShowMessages.error('متراژ قطعه رو وارد نکردین.', context);
        break;
      case 'parking':
        ShowMessages.error('وضعیت پاریکنیگ مشخص نیست', context);
        break;
      case 'bedroom':
        ShowMessages.error('تعداد اتاق های خواب رو وارد نکردین.', context);
        break;
      case 'bathroom':
        ShowMessages.error('تعداد سرویس های بهداشتی قطعه رو وارد نکردین.', context);
        break;
      case 'address':
        ShowMessages.error('لطفا آدرس قطعه رو وارد کنید.', context);
        break;
      case 'image':
        ShowMessages.error('شما باید حداقل یک عکس بارگذاری کنید.', context);
        break;
    }
  }

  String checkFiled(
      String price,
      String area,
      String parking,
      String bedroom,
      String address,
      String description,
      List<ImageId> images,
      String bathroom) {
    if (images.length == 0) {
      return 'image';
    } else if (price == null || price.isEmpty) {
      return 'price';
    } else if (area == null || area.isEmpty) {
      return 'area';
    } else if (parking == null || parking.isEmpty) {
      return 'parking';
    } else if (bedroom == null || bedroom.isEmpty) {
      return 'bedroom';
    } else if (bathroom == null || bathroom.isEmpty) {
      return 'bathroom';
    } else if (address == null || address.isEmpty) {
      return 'address';
    }
    return 'done';
  }

  String checkFiledEdit(
      String price,
      String area,
      String parking,
      String bedroom,
      String address,
      String description,
      List<ImageId> images,
      List<ProductImageLink> productImageLinks,String bathroom) {
    if (images.length + productImageLinks.length == 0) {
      return 'image';
    } else if (price == null || price.isEmpty) {
      return 'price';
    } else if (area == null || area.isEmpty) {
      return 'area';
    } else if (parking == null || parking.isEmpty) {
      return 'parking';
    } else if (bedroom == null || bedroom.isEmpty) {
      return 'bedroom';
    } else if (bathroom == null || bathroom.isEmpty) {
      return 'bathroom';
    } else if (address == null || address.isEmpty) {
      return 'address';
    }
    return 'done';
  }

  insertImage(
      String image, String imageName, String productId, String status) async {
    http.Response response = await http.post(
        Uri.encodeFull(MyRequest.baseUrl + 'insertProductImage.php'),
        body: <String, String>{
          'img': image,
          'imgname': imageName,
          'productId': productId,
          'status': status,
        });
    if(response.statusCode == 200){
      return 'done';
    }else{
      return '-1';
    }
    return response.body;
  }

  insertProduct(String status, String state, String city, String price, String area, String parking,
      String bedroom, String address, String description, String userid, String bathroom,String stateName,String cityName,String dateTime) async {

    Map<String, String> params = {
      'status': status,
      'state': state,
      'city': city,
      'stateName': stateName,
      'cityName': cityName,
      'price': price,
      'area': area,
      'parking': parking,
      'bedroom': bedroom,
      'address': address,
      'description': description ?? '',
      'userid': userid,
      'bathroom': bathroom,
      'dateTime':dateTime,
    };
    String id = await MyRequest.insert(params, 'insertProduct.php');
    return id;
  }

  editProduct(String pId, String status, String state, String city, String price, String area, String parking,
    String bedroom, String address, String description, String userid, String bathroom,String stateName,String cityName,List<Map<String,String>> deleteImg
  ) async {
    // print(json.encode(deleteImg));
    // print(userid);
    Map<String, String> params = {
      'id': pId,
      'status': status,
      'state': state,
      'city': city,
      'stateName': stateName,
      'cityName': cityName,
      'price': price,
      'area': area,
      'parking': parking,
      'bedroom': bedroom,
      'address': address,
      'description': description ?? '',
      'userid': userid,
      'bathroom': bathroom,
      'deleteImg':'${json.encode(deleteImg)}',
    };
    String id = await MyRequest.update(params, 'editProduct.php');

    return id;
  }
}
