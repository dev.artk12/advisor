import 'package:advisor/colors/colors.dart';
import 'package:advisor/moudles/numberconverter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleTextWhit extends StatelessWidget {
  final String text;
  final FontWeight fontWeight;
  final double fontSize;
  final Color color;
  final Function onTop;
  SimpleTextWhit(
      {this.text,
      this.fontWeight = FontWeight.normal,
      this.fontSize,
      this.color = Colors.white,
      this.onTop});

  @override
  Widget build(BuildContext context) {
    return Text(
      "$text",
      style: TextStyle(
          fontFamily: !NumberConverter.checkEnglish(text)?"iranian_sans":null,
          fontWeight: fontWeight,
          fontSize: fontSize,
          color: color),
    );
  }
}
