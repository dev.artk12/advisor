
import 'package:flutter/material.dart';

class AppColor{
  static final Color buttonBackgroundColor = Color.fromARGB(255, 56,56,56);
  static final Color buttonTextColor = Color.fromARGB(255, 224,224,224);
  static final Color textColor = Color.fromARGB(255, 52,61,70);
  static final Color signColorBackgroundColor = Color.fromARGB(220, 84,84,84);
}