import 'package:advisor/components/dateTimefunctions.dart';
import 'package:advisor/moudles/Rent.dart';
import 'package:advisor/moudles/product.dart';
import 'package:flutter/foundation.dart';

class RentController extends ChangeNotifier {
  List<String> sortingOption = ['جدیدترین', 'ارزانترین', 'گرانترین', 'متراژ'];
  List<Rent> rents = [];
  List<Rent> searchRents = [];
  int sortIndex = 1;
  Map<String, dynamic> filters = {};

  void removeMyProduct(String id){
    rents.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  void updateFilters(Map<String, dynamic> filters) {
    this.filters = filters;
    switch (sortIndex) {
      case 1:
        updateSortIndexOneByCalendar();
        break;
      case 2:
        updateSortIndexTwoByCheap();
        break;
      case 3:
        updateSortIndexThreeByExpensive();
        break;
      case 4:
        updateSortIndexFourByArea();
        break;
    }
    notifyListeners();
  }
  void addTOListFromMyProduct(Product product,String name,String mobile,String profileAddress) {
    searchRents.add(Rent(
        id: product.id,
        bathroom: product.bathroom,
        stateName: product.stateName,
        cityName: product.cityName,
        parking: product.parking,
        city: product.city,
        bedroom: product.bedroom,
        area: product.area,
        address: product.address,
        price: product.price,
        description: product.description,
        status: product.status,
        state: product.state,dateTime: product.dateTime,userId: product.userId,name: name,profileAddress: profileAddress,mobile: mobile));
    notifyListeners();
  }

  void editMyProductInList(Product product){

    rents.forEach((element) {
      if(element.id == product.id){
        element.stateName = product.stateName;
        element.state = product.state;
        element.city = product.city;
        element.cityName = product.cityName;
        element.address = product.address;
        element.description = product.description;
        element.bedroom = product.bedroom;
        element.bathroom = product.bathroom;
        element.area = product.area;
        element.parking = product.parking;
        element.price = product.price;
        return;
      }
    });
    notifyListeners();
  }

  void updateSortIndexOneByCalendar() {
    searchRents.sort((a, b) {
      DateTime dateTimeA = DateTimeFunctions.stringToDateTime(a.dateTime);
      DateTime dateTimeB = DateTimeFunctions.stringToDateTime(b.dateTime);
      return dateTimeB.compareTo(dateTimeA);
    });
  }

  void updateSortIndexTwoByCheap() {
    searchRents.sort((a, b) {
      return int.parse(a.price).compareTo(int.parse(b.price));
    });
  }

  void updateSortIndexThreeByExpensive() {
    searchRents.sort((a, b) {
      return int.parse(b.price).compareTo(int.parse(a.price));
    });
  }

  void updateSortIndexFourByArea() {
    searchRents.sort((a, b) {
      return int.parse(a.area).compareTo(int.parse(b.area));
    });
  }

  void update(List<Rent> rents) {
    this.rents = rents;
    this.searchRents = rents;
    updateSortIndexOneByCalendar();
  }

  void changeSortIndex(int i) {
    switch (i) {
      case 1:
        updateSortIndexOneByCalendar();
        break;
      case 2:
        updateSortIndexTwoByCheap();
        break;
      case 3:
        updateSortIndexThreeByExpensive();
        break;
      case 4:
        updateSortIndexFourByArea();
        break;
    }
    sortIndex = i;
    notifyListeners();
  }

  void onChangeSearchText(String val) {
    searchRents = [];
    rents.forEach((element) {
      if (element.price.contains(val) ||
          element.area.contains(val) ||
          element.address.replaceAll('آ', 'ا').contains(val) ||
          element.cityName.replaceAll('آ', 'ا').contains(val) ||
          element.stateName.replaceAll('آ', 'ا').contains(val)) {
        searchRents.add(element);
      }
    });

    switch (sortIndex) {
      case 1:
        updateSortIndexOneByCalendar();
        break;
      case 2:
        updateSortIndexTwoByCheap();
        break;
      case 3:
        updateSortIndexThreeByExpensive();
        break;
      case 4:
        updateSortIndexFourByArea();
        break;
    }

    notifyListeners();
  }


}
