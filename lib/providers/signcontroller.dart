
import 'dart:convert';
import 'dart:io';

import 'package:advisor/moudles/numberconverter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persian_number_utility/persian_number_utility.dart';

class SignController extends ChangeNotifier{

  String name = '';
  String phone = '';
  String smsCodeUser = '';
  File image;
  String imageString;
  String imageName;
  String imgLink='';
  String smsCode;
  Map<String,String> profile = {};
  int checkName;
  int checkPhone;
  int checkPass;
  int checkConfirmPass;

  void onChangeSmsCodeUser(String val){
    this.smsCodeUser = NumberConverter.convertDigitsToLatin(val);
  }

  String getImageString(File image) {
    List<int> imageBytes = image.readAsBytesSync();
    return base64Encode(imageBytes);
  }

  String getImageName(String id) {
    DateTime dateTime = DateTime.now();
    return '${dateTime.year}${dateTime.month}${dateTime.day}${dateTime.hour}'
        '${dateTime.minute}${dateTime.second}${dateTime.millisecond}_profile_$id';
  }

  void onChangeImage(File image){
    this.image = image;
    // imageString = getImageString(image);
    // imageName = getImageName();
    // profile['image'] = imageString;
    // profile['imageName'] = imageName;
    notifyListeners();
  }

  void onChangeName(String name){
    this.name = name;
    profile['name'] = name;
    if(name.isEmpty){
      checkName = 1;
    }else if(!validateName()){
      checkName = 2;
    }else{
      checkName = 3;
    }
    notifyListeners();
  }
  
  void onChangePhone(String phone){
    this.phone = NumberConverter.convertDigitsToLatin(phone);
    profile['phone'] = this.phone;
    if(name.isEmpty){
      checkPhone = 1;
    }else if(!validatePhone()){
      checkPhone = 2;
    }else{
      checkPhone = 3;
    }
    notifyListeners();
  }



  //------------checking-----------

  bool validateName(){
    return name.length>2?true:false;
  }

  bool validatePhone(){
    print(phone.substring(0,2));
    if(phone.length == 11 && phone.substring(0,2) == '09'||phone.substring(0,2) =='٠٩'){
      return true;
    }
    return false;
  }

  // bool validatePass(){
  //   return pass.length>3?true:false;
  // }
  //
  // bool validateConfirmPass(){
  //   return pass == confirmPass?true:false;
  // }

  String checkingFields(){
    if(checkName != 3){
      return 'name';
    }else if(checkPhone != 3){
      return 'phone';
    }else if(checkPass != 3){
      return 'pass';
    }else if(checkConfirmPass != 3){
      return 'confirmPass';
    }else{
      return 'done';
    }
  }


  void showMessage(String val , BuildContext context){
    if(val == 'name'){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('name'),));
    }else if(val == 'phone'){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('phone'),));
    }else if(val == 'pass'){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('pass'),));
    }else if(val == 'confirmPass'){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('ConfirmPass'),));
    }
  }



}