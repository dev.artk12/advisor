import 'package:advisor/components/dateTimefunctions.dart';
import 'package:advisor/moudles/product.dart';
import 'package:advisor/moudles/sales.dart';
import 'package:flutter/foundation.dart';

class SalesController extends ChangeNotifier {
  List<String> sortingOption = ['جدیدترین', 'ارزانترین', 'گرانترین', 'متراژ'];
  List<Sale> sales = [];
  List<Sale> searchSales = [];
  int sortIndex = 1;
  Map<String, dynamic> filters = {};

  void removeMyProduct(String id){
    sales.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  void updateFilters(Map<String, dynamic> filters) {
    this.filters = filters;

    switch (sortIndex) {
      case 1:
        updateSortIndexOneByCalendar();
        break;
      case 2:
        updateSortIndexTwoByCheap();
        break;
      case 3:
        updateSortIndexThreeByExpensive();
        break;
      case 4:
        updateSortIndexFourByArea();
        break;
    }
    notifyListeners();
  }

  void updateSortIndexOneByCalendar() {
    searchSales.sort((a, b) {
      DateTime dateTimeA = DateTimeFunctions.stringToDateTime(a.dateTime);
      DateTime dateTimeB = DateTimeFunctions.stringToDateTime(b.dateTime);
      return dateTimeB.compareTo(dateTimeA);
    });
  }

  void updateSortIndexTwoByCheap() {
    searchSales.sort((a, b) {
      return int.parse(a.price).compareTo(int.parse(b.price));
    });
  }
  void editMyProductInList(Product product){

    sales.forEach((element) {
      if(element.id == product.id){
        element.stateName = product.stateName;
        element.state = product.state;
        element.city = product.city;
        element.cityName = product.cityName;
        element.address = product.address;
        element.description = product.description;
        element.bedroom = product.bedroom;
        element.bathroom = product.bathroom;
        element.area = product.area;
        element.parking = product.parking;
        element.price = product.price;
        return;
      }
    });
    notifyListeners();
  }

  void updateSortIndexThreeByExpensive() {
    searchSales.sort((a, b) {
      return int.parse(b.price).compareTo(int.parse(a.price));
    });
  }

  void updateSortIndexFourByArea() {
    searchSales.sort((a, b) {
      return int.parse(a.area).compareTo(int.parse(b.area));
    });
  }

  void update(List<Sale> sales) {
    this.sales = sales;
    this.searchSales = sales;
    updateSortIndexOneByCalendar();
  }

  void addTOListFromMyProduct(Product product,String name,String mobile,String profileAddress) {
    searchSales.add(Sale(
        id: product.id,
        bathroom: product.bathroom,
        stateName: product.stateName,
        cityName: product.cityName,
        parking: product.parking,
        city: product.city,
        bedroom: product.bedroom,
        area: product.area,
        address: product.address,
        price: product.price,
        description: product.description,
        status: product.status,
        state: product.state,dateTime: product.dateTime,userId: product.userId,name: name,profileAddress: profileAddress,mobile: mobile));
    notifyListeners();
  }

  void changeSortIndex(int i) {
    switch (i) {
      case 1:
        updateSortIndexOneByCalendar();
        break;
      case 2:
        updateSortIndexTwoByCheap();
        break;
      case 3:
        updateSortIndexThreeByExpensive();
        break;
      case 4:
        updateSortIndexFourByArea();
        break;
    }
    sortIndex = i;
    notifyListeners();
  }

  void onChangeSearchText(String val) {
    searchSales = [];
    sales.forEach((element) {
      if (element.price.contains(val) ||
          element.area.contains(val) ||
          element.address.replaceAll('آ', 'ا').contains(val) ||
          element.cityName.replaceAll('آ', 'ا').contains(val) ||
          element.stateName.replaceAll('آ', 'ا').contains(val)) {
        searchSales.add(element);
      }
    });

    switch (sortIndex) {
      case 1:
        updateSortIndexOneByCalendar();
        break;
      case 2:
        updateSortIndexTwoByCheap();
        break;
      case 3:
        updateSortIndexThreeByExpensive();
        break;
      case 4:
        updateSortIndexFourByArea();
        break;
    }

    notifyListeners();
  }
}
