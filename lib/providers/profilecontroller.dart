import 'package:advisor/moudles/product.dart';
import 'package:advisor/moudles/user.dart';
import 'package:flutter/cupertino.dart';

class ProfileController extends ChangeNotifier {
  List<Product> products = new List();
  bool needToRefreshProductList = true;
  bool profileChanged = true;
  // String name;
  // String des;
  // String profileAddress;
  // String id;
  String phoneNumber;
  int length = 0;
  Product product;
  bool isDispose = false;

  // void update(User user){
  //   this.name = user.name;
  //   this.profileAddress = user.profileAddress;
  //   this.des = user.description;
  //   this.phoneNumber = user.mobileNumber;
  // }

  void addProduct(Product product) {
    // this.product = product;
    products.add(product);
    if (!isDispose) {
      notifyListeners();
    }
  }

  void removeProduct(String id) {
    // this.product = product;
    products.removeWhere((element) => element.id == id);
    notifyListeners();

  }

  void editProduct(Product product) {

    for(int i = 0 ; i < products.length;i++){
      if(product.id == products[i].id){
        Product p = products[i];
        p.bathroom = product.bathroom;
        p.parking = product.parking;
        p.bedroom = product.bedroom;
        p.address = product.address;
        p.price = product.price;
        p.area = product.area;
        p.cityName = product.cityName;
        p.city = product.city;
        p.state = product.state;
        p.description = product.description;
        break;
        // return;
      }
    }
    // Product p = products.firstWhere((element) => element.id == product.id);
    // String userid = p.userId;
    // String status = product.status;
    // String dateTime = product.dateTime;
    //
    // products.removeWhere((element) => element.id == p.id);
    // products.add(Product(
    //     id:product.id,
    //     bathroom: product.bathroom,
    //     stateName: product.stateName,
    //     parking: product.parking,
    //     bedroom: product.bedroom,
    //     address: product.address,
    //     price: product.price,
    //     area: product.area,
    //     cityName: product.cityName,
    //     city: product.city,
    //     state: product.state,
    //     dateTime: dateTime,
    //     description: product.description,
    //     userId: userid,
    //     status: status));
    notifyListeners();
  }

  void updateProductsLength(int length) {
    this.length = length;
    product = null;
    if (!isDispose) {
      notifyListeners();
    }
  }

  void updateToRefresh() {
    needToRefreshProductList = true;
    if (!isDispose) {
      notifyListeners();
    }
  }

  void setProducts(List<Product> products) {
    this.products = products;
    if (!isDispose) {
      // notifyListeners();
    }
  }

  @override
  void dispose() {
    isDispose = true;
    super.dispose();
  }
}
