
import 'package:flutter/cupertino.dart';

class FilterController extends ChangeNotifier{

  Map<String,dynamic> filter = {};
  bool parking = false;
  bool noParking = false;
  String startPrice = '';
  String endPrice = '';
  String startBedroom = '';
  String endBedroom = '';
  String startArea = '';
  String endArea = '';

  void onChangeStartPrice(String val){
    this.startPrice = val;
    filter['startPrice'] = val;
  }

  void onChangeStartBedroom(String val){
    this.startBedroom = val;
    filter['startBedroom'] = val;
  }

  void onChangeStartArea(String val){
    this.startArea = val;
    filter['startArea'] = val;
  }

  void onChangeEndPrice(String val){
    this.endPrice = val;
    filter['endPrice'] = val;
  }

  void onChangeEndBedroom(String val){
    this.endBedroom = val;
    filter['endBedroom'] = val;
  }

  void onChangeEndArea(String val){
    this.endArea = val;
    filter['endArea'] = val;
  }

  void onChangeParking(bool parking){
    this.parking = parking;
    filter['parking'] = parking;
    notifyListeners();
  }

  void onChangeNoParking(bool noParking){
    this.noParking = noParking;
    filter['noParking'] = noParking;
    notifyListeners();
  }

  void updateFilter(Map<String,dynamic> filter){
    if(filter.isNotEmpty) {
      this.parking = filter['parking']??false;
      this.noParking = filter['noParking']??false;
      this.startPrice = filter['startPrice']??'';
      this.startArea = filter['startArea']??'';
      this.startBedroom = filter['startBedroom']??'';
      this.endPrice = filter['endPrice']??'';
      this.endArea = filter['endArea']??'';
      this.endBedroom = filter['endBedroom']??'';
    }
  }

}