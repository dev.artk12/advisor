
import 'dart:io';
import 'package:advisor/moudles/imageid.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:flutter/cupertino.dart';

class ProductController extends ChangeNotifier{

  List<ImageId> images = new List();
  List<ProductImageLink> imagesLink = new List();
  int firstImageLoadLength;
  List<Map<String,String>> deleteImagedId=[];
  String id;
  bool parkingCheck = false;
  int status = 1;
  int state = 1;
  int city = 1;
  String price;
  String parking;
  String bedRoom;
  String area;
  String address;
  String description;
  String bathroom;
  bool request = false;
  int i = 0;
  String productId;
  // String state = CityAndStates().states[controller.state-1];
  // String city = CityAndStates().getStateCity(controller.state)[controller.city-1];


  void updateRequest(bool check){
    request = check;
    notifyListeners();
  }

  void updateI(){
    i++;
    notifyListeners();
  }

  void updateProductId(String id){
    this.productId = id;
    notifyListeners();
  }

  void update(String id,String status,String state,String city,String price,String parking,
      String bedRoom,String area,String address,String description ,List<ProductImageLink> imagesLink,String bathroom){
    this.status = int.parse(status);
    this.state = int.parse(state);
    this.city = int.parse(city);
    this.id = id;
    this.imagesLink = imagesLink;
    this.firstImageLoadLength = imagesLink.length;
    this.price = price;
    this.parking = parking;
    if(int.parse(parking) > 0){
      parkingCheck = true;
    }
    this.bedRoom = bedRoom;
    this.area = area;
    this.address = address;
    this.description = description;
    this.bathroom = bathroom;
  }

  void updateParkingCheck(bool check){
    if(check){
      parking = '1';
    }else{
      parking = '0';
    }
    this.parkingCheck = check;
    notifyListeners();
  }

  void changeBathroom(String i){
    this.bathroom = i;
  }

  void deleteImgLink(String id,String link){
    imagesLink.removeWhere((element) => element.id == id);
    deleteImagedId.add({'id':'$id','link':'$link'});
    notifyListeners();
  }

  void addImage(ImageId file){
    images.add(file);
    notifyListeners();
  }

  void deleteImage(String path){
    images.removeWhere((element) => element.image.path == path);
    notifyListeners();
  }

  void changeStatus(int i ){
    status = i;
    notifyListeners();
  }

  void changeState(int i ){
    state = i;
    notifyListeners();
  }

  void changeCity(int i ){
    city = i;
    notifyListeners();
  }

  void changePrice(String i ){
    price = i;
    // print(price);
    notifyListeners();
  }

  void changeParking(String i ){
    parking = i;
    // notifyListeners();
  }

  void changeBedRoom(String i ){
    bedRoom = i;
    // notifyListeners();
  }

  void changeArea(String i ){
    area = i;
    // notifyListeners();
  }

  void changeAddress(String i ){
    address = i;
    notifyListeners();
  }

  void changeDescription(String i ){
    description = i;
    notifyListeners();
  }

}