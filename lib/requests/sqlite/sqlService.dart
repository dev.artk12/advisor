
import 'package:advisor/colors/colors.dart';
import 'package:advisor/components/simpletextwhit.dart';
import 'package:advisor/moudles/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';


class SqLiteService {

  static void showStatus(String message,BuildContext context){
    Scaffold.of(context).showSnackBar(SnackBar(backgroundColor: AppColor.textColor,content: SimpleTextWhit(text:'در حال ذخیره ....'),));
  }

  Future<int> insert(Map<String,dynamic> map)async{
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'user.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = database;
    dynamic res = await db.insert('user', map,conflictAlgorithm: ConflictAlgorithm.replace);
    return res;
  }

  static Future<void> update(Map<String,dynamic> map,int id)async{
    print(id.toString());
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'user.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = database;
    await db.update('user', map,where: 'ID = ?',whereArgs: [id]);
  }

  Future<void> delete(int id)async{
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'user.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = database;
    await db.delete('user',where: 'ID = ?',whereArgs: [id]);
  }

  Future<User> getUser()async{
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'user.db');
    Database db = await openDatabase(path, version: 1);
    final List<Map<String, dynamic>> maps = await db.query('user');
    return User().fromJson(maps[0]);
  }

  static Future<void> createDBIfUserExists() async {
    openDatabase(
      join(await getDatabasesPath(), 'user.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE IF NOT EXISTS user ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(300) NOT NULL ,profileaddress VARCHAR(400) NOT NULL , mobilenumber VARCHAR(13) NOT NULL,description TEXT NOT NULL)",
        );
      },
      version: 1,
    );
  }

}