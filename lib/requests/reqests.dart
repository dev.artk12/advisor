
import 'dart:convert';
import 'dart:io';

import 'package:advisor/moudles/product.dart';
import 'package:advisor/moudles/product_image_link.dart';
import 'package:advisor/moudles/sales.dart';
import 'package:advisor/moudles/user.dart';
import 'package:http/http.dart' as http;
import 'package:persian_number_utility/persian_number_utility.dart';
import '../moudles/Rent.dart';

class MyRequest{


  static String baseUrl = "http://realestatedealer.ir/advisor/";

  static Future<String> insert(Map<String,String> map , String url)async{
    map.forEach((key, value) {
      map.update(key, (value) => NumberUtility.changeDigit(value, NumStrLanguage.English));
    });
    try{
      final response = await http.post(baseUrl+url,body: map);
      print(response.statusCode);
      if(response.statusCode == 200){
        return response.body;
      }else{
        return '-1';
      }
    }on SocketException {
      return '-1';
    }on Exception{
      return '-1';
    }
  }

  static Future<String> update(Map<String,String> map , String url)async{

    map.forEach((key, value) {
      map.update(key, (value) => NumberUtility.changeDigit(value, NumStrLanguage.English));
    });
    try{
      final response = await http.post(baseUrl+url,body: map);
      if(response.statusCode == 200){
        return response.body;
      }else{
        return '-1';
      }
    }on SocketException {
      return '-1';
    }on Exception{
      return '-1';
    }

  }

  static Future<String> delete(Map<String,String> map , String url)async{
    map.forEach((key, value) {
      map.update(key, (value) => NumberUtility.changeDigit(value, NumStrLanguage.English));
    });
    try{
      final response = await http.post(baseUrl+url,body: map);
      if(response.statusCode == 200){
        return response.body;
      }else{
        return '-1';
      }
    }on SocketException {
      return '-1';
    }on Exception{
      return '-1';
    }

  }

  static Future<String> updateName(Map<String,String> map , String url)async{
    map.forEach((key, value) {
      key = NumberUtility.changeDigit(value, NumStrLanguage.English);
    });
    final response = await http.post(baseUrl+url,body: map);
    if(response.statusCode == 200){
      return response.body;
    }else{
      return '-1';
    }
  }

  static Future<User> getUser(String id)async{
    final response = await http.post(baseUrl+'getUserDetail.php',body: {'userId':id});
    if(response.statusCode == 200){
      return User().getUserAccount(response.body);
    }else{
      return null;
    }

  }

  static Future<List<Product>> getMyProduct(String userId) async {
     final response = await http.post(baseUrl+'getMyProduct.php',body: {'id':userId});
     return Product().getProductListFromJson(response.body);
  }

  static Future<List<Sale>> getSalesProducts() async {
    final response = await http.get(baseUrl+'getSales.php');
    return Sale().getSaleListFromJson(response.body);
  }

  static Future<List<Rent>> getRentProducts() async {
    final response = await http.get(baseUrl+'getRents.php');
    return Rent().getRentListFromJson(response.body);
  }

  static Future<List<ProductImageLink>> getMyProductImageLink(String productId,String status) async {
    Map<String,String> map = {'productId':productId,'status':status};
    // print(status);
    final response = await http.post(baseUrl+'getProductImage.php',body: map);
    return ProductImageLink().getProductListFromJson(response.body);
  }

  static Future<String> deleteProductImage(String imageId,String imageAddress,String status) async {
    Map<String,String> map = {'id':imageId,'imglink':imageAddress,'status':status};
    final response = await http.post(baseUrl+'deleteproductimage.php',body: map);
    return response.body;
  }
}