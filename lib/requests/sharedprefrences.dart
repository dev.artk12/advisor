import 'package:advisor/moudles/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MySharedPreferences {

  static insertSign()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('sign', true);
  }

  static Future<bool> getSign()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool sign = prefs.getBool('sign')??false;
    return sign;
  }
  // static Future<int> getId()async{
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   int id = prefs.getInt('id')??-1;
  //   return id;
  // }

  // static Future<User> getUser()async{
  //   String name = await getName();
  //   int id = await getId();
  //   return User(id: id,name: name);
  // }

  // static insertUser(int id,String name )async{
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.setInt('id', id);
  //   prefs.setString('name', name);
  // }

  // static deleteUser()async{
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.remove('id');
  //   prefs.remove('name');
  // }
}